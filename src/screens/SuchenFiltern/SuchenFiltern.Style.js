import {StyleSheet, Dimensions} from 'react-native';
import {fonts, colors} from '../../styles/theme';
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white_color,
  },
  topViewStyle: {
    width: screenWidth,
    // height: screenHeight / 4,
    backgroundColor: colors.white_color,
  },
  topLineViewStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 55,
    marginBottom: 25,
    // marginLeft: 20,
  },
  iconStyle: {
    width: 20,
    height: 20,
    color: colors.black_color,
  },

  logoStyle: {
    width: 86,
    height: 86,
    marginTop: -60,
  },
  heading: {
    fontWeight: '700',
    fontSize: 20,
    marginTop: 2,
    lineHeight: 22,
    textAlign: 'center',
    letterSpacing: -0.408,
    color: colors.font_color,
    fontFamily: fonts.bold_font,
  },
  subHeading: {
    fontWeight: '600',
    fontSize: 16,
    lineHeight: 22,
    textAlign: 'center',
    letterSpacing: -0.408,
    color: colors.font_color,
    fontFamily: fonts.semibold_font,
    marginBottom: 12,
  },
  subTitle: {
    fontWeight: '700',
    fontSize: 16,
    lineHeight: 22,
    letterSpacing: -0.408,
    color: colors.font_color,
    fontFamily: fonts.bold_font,
    marginTop: 30,
    textAlign: 'center',
  },
  content: {
    fontWeight: '600',
    fontSize: 12,
    lineHeight: 22,
    textAlign: 'right',
    opacity: 0.7,
    letterSpacing: -0.408,
    color: colors.font_color,
    fontFamily: fonts.semibold_font,
    marginRight: 8,
  },
  spacingHead: {
    marginTop: 30,
  },
  spacingSubHead: {
    marginTop: 25,
  },
  padBottom: {
    paddingBottom: 20,
  },
  slider: {
    flex: 1,
    // width: screenWidth / 1.6,
    marginLeft: 10,
    marginRight: 10,
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  width: {
    width: screenWidth / 1.35,
    alignItems: 'center',
    marginTop: -10,
  },
  reduceMarginTop: {
    marginTop: -10,
  },
  sliderLine: {
    height: 8,
    // backgroundColor: colors.font_color,
    borderRadius: 16,
    borderWidth: 1.5,
    borderColor: colors.font_color,
  },
  thumbStyle: {
    backgroundColor: '#FF6536',
    borderRadius: 16,
    borderWidth: 1.5,
    borderColor: '#2e2e2e80',
    height: 18,
    width: 18,
  },
  trackStyle: {
    backgroundColor: 'white',
    height: 6,
  },
  rightPad: {
    marginRight: 10,
  },
  leftPadd: {
    marginLeft: 10,
  },
  rectanglebox: {
    height: 16,
    width: 24,
    borderRadius: 15,
    borderWidth: 1.5,
    borderColor: '#2e2e2e4d',
    marginLeft: 8,
  },
  bgYellow: {
    backgroundColor: '#FFE600',
  },
  bgblue: {
    backgroundColor: '#00A3FF',
  },
  bgblack: {
    backgroundColor: '#2E2E2E',
  },
  valueStyle: {
    fontWeight: '600',
    fontSize: 12,
    lineHeight: 22,
    textAlign: 'right',
    opacity: 0.7,
    letterSpacing: -0.408,
    color: colors.font_color,
    fontFamily: fonts.semibold_font,
  },
});
