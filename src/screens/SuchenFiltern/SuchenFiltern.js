import React from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StatusBar,
  ScrollView,
  TextInput,
  FlatList,
  AppRegistry,
  StyleSheet,
} from 'react-native';
import Slider from 'react-native-sliders';
import Divider from '../../components/Divider';
import Stars from './Starts';
import styles from './SuchenFiltern.Style';
import commonStyle from '../../styles/common.Style';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import IconI from 'react-native-vector-icons/Ionicons';
import { colors } from '../../styles/theme';
const SuchenFiltern = ({ navigation }) => {
  const start_data = [1, 2, 3, 4, 5];
  const df = [3, 7];
  const [value, setValue] = React.useState([30, 60]);
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="transparent" barStyle="dark-content" />
      <View style={styles.topViewStyle}>
        <View style={[styles.topLineViewStyle]}>
          <Image
            source={require('../../assets/images/logo.png')}
            style={styles.logoStyle}
            resizeMode="contain"
          />
        </View>
      </View>
      <ScrollView>
        <View style={[commonStyle.padhorzontal, styles.padBottom]}>
          <View style={[commonStyle.row, commonStyle.spaceBtw]}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <IconI color="black" name="chevron-back-circle" size={25} />
            </TouchableOpacity>
            <Text style={styles.heading}>Suchen & Filtern</Text>
            <IconI color="black" name="close-circle" size={25} />
          </View>
          <TouchableOpacity onPress={() => navigation.navigate('StoreView1')}
            style={[commonStyle.row, commonStyle.spaceBtw, styles.spacingHead]}>
            <Text style={styles.subHeading}>Kategorie</Text>
            <View
              style={[
                commonStyle.row,
                commonStyle.alignCenter,
                styles.reduceMarginTop,
              ]}>
              <Text style={styles.content}>Alle</Text>
              <Icon name="arrow-right" size={14} color={colors.font_color} />
            </View>
          </TouchableOpacity>
          <Divider />
          <View
            style={[
              commonStyle.row,
              commonStyle.spaceBtw,
              styles.spacingSubHead,
            ]}>
            <Text style={styles.subHeading}>Farbe</Text>
            <View style={commonStyle.row}>
              <View style={[styles.rectanglebox, styles.bgblue]} />
              <View style={[styles.rectanglebox, styles.bgblack]} />
              <View style={[styles.rectanglebox, styles.bgYellow]} />
            </View>
          </View>
          <Divider />
          <View
            style={[
              commonStyle.row,
              commonStyle.spaceBtw,
              styles.spacingSubHead,
            ]}>
            <Text style={styles.subHeading}>Preis</Text>
            <View style={[commonStyle.row, styles.width]}>
              <Text style={[styles.rightPad, styles.valueStyle]}>50 €</Text>
              <View style={[styles.container]}>
                <Slider
                  value={value}
                  maximumValue={100}
                  step={1}
                  maximumTrackTintColor="#FF6536"
                  minimumTrackTintColor="#FF6536"
                  thumbStyle={styles.thumbStyle}
                  style={styles.sliderLine}
                  trackStyle={styles.trackStyle}
                  minimumValue={0}
                  onValueChange={value => setValue(value)}
                />
              </View>
              <Text style={[styles.leftPadd, styles.valueStyle]}>100 €</Text>
            </View>
            {/* <Text style={styles.content}>Alle</Text> */}
          </View>
          <Divider />
          <View
            style={[
              commonStyle.row,
              commonStyle.spaceBtw,
              styles.spacingSubHead,
            ]}>
            <Text style={styles.subHeading}>Material</Text>
            <View
              style={[
                commonStyle.row,
                commonStyle.alignCenter,
                styles.reduceMarginTop,
              ]}>
              <Text style={styles.content}>Alle</Text>
              <Icon name="arrow-right" size={14} color={colors.font_color} />
            </View>
          </View>
          <Divider />
          <View
            style={[
              commonStyle.row,
              commonStyle.spaceBtw,
              styles.spacingSubHead,
            ]}>
            <Text style={styles.subHeading}>Style</Text>
            <View
              style={[
                commonStyle.row,
                commonStyle.alignCenter,
                styles.reduceMarginTop,
              ]}>
              <Text style={styles.content}>Alle</Text>
              <Icon name="arrow-right" size={14} color={colors.font_color} />
            </View>
          </View>
          <Divider />
          <Text style={styles.subTitle}>Bewertungen</Text>
          <TouchableOpacity onPress={() => navigation.navigate('Feedback')}>
            <Stars data={start_data} endTitle="ab" end={5} />
          </TouchableOpacity>
          <Divider />
          <TouchableOpacity onPress={() => navigation.navigate('Feedback')}>
            <Stars data={start_data} endTitle="ab" end={4} />
          </TouchableOpacity>
          <Divider />
          <TouchableOpacity onPress={() => navigation.navigate('Feedback')}>
            <Stars data={start_data} endTitle="ab" end={3} />
          </TouchableOpacity>
          <Divider />
          <TouchableOpacity onPress={() => navigation.navigate('Feedback')}>
            <Stars data={start_data} endTitle="ab" end={2} />
          </TouchableOpacity>
          <Divider />
          <TouchableOpacity onPress={() => navigation.navigate('Feedback')}>
            <Stars data={start_data} endTitle="ab" end={1} />
          </TouchableOpacity>
          <Divider />
        </View>
      </ScrollView>
    </View>
  );
};
AppRegistry.registerComponent('SuchenFiltern', () => SuchenFiltern);
export default SuchenFiltern;
