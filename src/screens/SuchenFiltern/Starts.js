import React from 'react';
import {View, Text, StyleSheet, FlatList} from 'react-native';
import PropTypes from 'prop-types';
import {fonts, colors} from '../../styles/theme';
import Icon from 'react-native-vector-icons/Entypo';

const Stars = props => {
  return (
    <View style={props?.marginDisable == true ? {} : styles.margin}>
      <FlatList
        data={props.data}
        horizontal
        renderItem={({item: rowData}) => {
          return (
            <View style={styles.row}>
              <Icon
                style={styles.startStyle}
                name="star"
                size={props?.size ? props?.size : 20}
                color={rowData <= props.end ? '#FF6536' : '#DADADA'}
              />
              {rowData == 5 && (
                <Text style={styles.endTitle}>{props.endTitle ?? ''}</Text>
              )}
            </View>
          );
        }}
        keyExtractor={(item, index) => index}
      />
    </View>
  );
};

// Stars.propTypes = {
//   data: PropTypes.array.isRequired,
//   endTitle: PropTypes.string,
// };
export default Stars;
const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
  },
  startStyle: {
    marginRight: 5,
  },
  endTitle: {
    marginLeft: 10,
    fontWeight: '600',
    fontSize: 12,
    lineHeight: 22,
    opacity: 0.5,
    letterSpacing: -0.408,
    color: colors.font_color,
    fontFamily: fonts.semibold_font,
  },
  margin: {
    marginBottom: 8,
    marginTop: 20,
  },
});
