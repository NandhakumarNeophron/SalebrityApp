import React, {useEffect, useRef} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StatusBar,
  ScrollView,
  FlatList,
  LogBox,
  Dimensions,
  ImageBackground,
} from 'react-native';
import styles from './StoreView1.Style';
import commonStyle from '../../styles/common.Style';
import RBSheet from 'react-native-raw-bottom-sheet';
import BottomSheet from '../Feedback/BottomSheet';
const StoreView1 = ({navigation}) => {
  const refRBSheet = useRef();
  const screenWidth = Math.round(Dimensions.get('window').width);
  const listData = [
    {
      id: '1',
      listImage: require('../../assets/images/list_image6.png'),
      roundImage: require('../../assets/images/round_image1.png'),
      title: 'Burberry Jackie',
      subTitle: 'Jacke',
      size: 'XS',
      EUR: '0',
      USA: '0',
      price: '639',
      wishCount: '5',
      isTop: '0',
      cart: '3',
    },
    {
      id: '2',
      listImage: require('../../assets/images/list_image7.png'),
      roundImage: require('../../assets/images/round_image2.png'),
      title: 'Nike Air Force',
      subTitle: 'Schuhe',
      size: '40',
      EUR: '0',
      USA: '0',
      price: '90',
      wishCount: '23',
      isTop: '1',
      cart: '5',
    },
    {
      id: '3',
      listImage: require('../../assets/images/list_image8.png'),
      roundImage: require('../../assets/images/round_image3.png'),
      title: 'Nike Sneaker',
      subTitle: 'Schuhe',
      size: '43',
      EUR: '0',
      USA: '2xx24',
      price: '56',
      wishCount: '21',
      isTop: '0',
      cart: '1',
    },
    {
      id: '4',
      listImage: require('../../assets/images/list_image9.png'),
      roundImage: require('../../assets/images/round_image4.png'),
      title: 'Baby Winteroverall',
      subTitle: 'Kinder Kleidung',
      size: '',
      EUR: '98',
      USA: '',
      price: '100',
      wishCount: '39',
      isTop: '1',
      cart: '7',
    },
    {
      id: '5',
      listImage: require('../../assets/images/list_image10.png'),
      roundImage: require('../../assets/images/round_image1.png'),
      title: 'Tasche',
      subTitle: 'Tasche',
      size: 'XS',
      EUR: '',
      USA: '',
      price: '56',
      wishCount: '25',
      isTop: '0',
      cart: '8',
    },
  ];
  useEffect(() => {
    LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
  }, []);
  const moveOnStore = async (item) => {
    navigation.navigate('StoreDetails', {
      storeValues: item,
    });
 };
  return (
    <View style={styles.container}>
      <StatusBar
        backgroundColor="transparent"
        barStyle="dark-content"
        translucent={true}
      />
      <View style={styles.topViewStyle}>
        <View style={styles.topLineViewStyle}>
          {/* <Image source={require('../../assets/images/backbutton_icon.png')} style={styles.iconStyle} resizeMode='contain' />
                    <View style={styles.logoViewStyle}> */}
          <Image
            source={require('../../assets/images/logo.png')}
            style={styles.logoStyle}
            resizeMode="contain"
          />
          {/* </View>
                    <View style={{ position: 'absolute', flexDirection: 'row', right: 20 }}>
                        <Image source={require('../../assets/images/search_icon.png')} style={styles.iconStyle} resizeMode='contain' />
                    </View> */}
        </View>
        <View
          style={{
            ...commonStyle.row,
            ...commonStyle.spaceBtw,
            marginTop: -25,
            marginHorizontal: 20,
          }}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Image
              source={require('../../assets/images/backbutton_icon.png')}
              style={styles.iconStyle}
              resizeMode="contain"
            />
          </TouchableOpacity>
          <Image
            source={require('../../assets/images/search_icon.png')}
            style={styles.iconStyle}
            resizeMode="contain"
          />
        </View>
        <View
          style={{flexDirection: 'row', marginTop: 25, alignSelf: 'center'}}>
          <TouchableOpacity
            style={[styles.horizontalbuttonStyle, {marginLeft: -5}]}>
            <Text style={styles.horizontalbuttonText}>Alle</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.horizontalbuttonStyle}>
            <Text style={styles.horizontalbuttonText}>Beige</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.horizontalbuttonStyle}>
            <Text style={styles.horizontalbuttonText}>XS</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.horizontalbuttonStyle}>
            <Text style={styles.horizontalbuttonText}>Other</Text>
          </TouchableOpacity>
        </View>
      </View>
      <ScrollView style={{marginTop: -20}} showsVerticalScrollIndicator={false}>
        <View style={styles.centerViewStyle}>
          <Text style={styles.headerText}>123 332 Thousand Produkte</Text>
          <View style={styles.headerView}>
            <TouchableOpacity
              onPress={() => navigation.navigate('FilterView')}
              style={styles.roundView}>
              <Image
                source={require('../../assets/images/filter_icon.png')}
                style={styles.filterImageStle}
                resizeMode="contain"
              />
              <Text style={styles.filterText}>Alle Filter</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.roundView, {position: 'absolute', right: 0}]}
              onPress={() => refRBSheet.current.open()}>
              <Text style={styles.filterText}>Sortieren </Text>
              <Image
                source={require('../../assets/images/down_arrow_icon.png')}
                style={styles.filterImageStle}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
          <FlatList
            showsVerticalScrollIndicator={false}
            data={listData}
            removeClippedSubviews={true}
            maxToRenderPerBatch={8}
            windowSize={10}
            initialNumToRender={8}
            keyExtractor={({id}) => `key-${id}`}
            renderItem={({item, index}) => (
              <View style={styles.productView}>
                <TouchableOpacity
                  style={styles.productBoxView}
                  onPress={() => moveOnStore(item)}>
                  <View style={{position: 'absolute', left: -2}}>
                    <Image
                      source={item.listImage}
                      style={styles.boxImageStyle}
                      resizeMode="cover"
                    />
                  </View>
                  <View
                    style={{position: 'absolute', left: screenWidth / 2.85}}>
                    <Text
                      style={[styles.filterText, {width: screenWidth / 2.65}]}
                      numberOfLines={1}>
                      {item.title}
                    </Text>
                    {item.size != '' && (
                      <Text style={styles.filterSizeText}>
                        Size: {item.size}
                      </Text>
                    )}
                    {item.EUR > 0 && (
                      <Text style={styles.filterSizeText}>EUR: {item.EUR}</Text>
                    )}
                    {item.USA > 0 && (
                      <Text style={styles.filterSizeText}>USA: {item.USA}</Text>
                    )}
                    <Text style={styles.filterText}>{item.price} €</Text>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      position: 'absolute',
                      right: 0,
                      justifyContent: 'center',
                    }}>
                    <View
                      style={{
                        marginLeft: 30,
                        marginRight: 15,
                        justifyContent: 'center',
                      }}>
                      {item.isTop == 1 ? (
                        <ImageBackground
                          source={require('../../assets/images/heart_icon.png')}
                          style={styles.bgImageStyle}
                          resizeMode="contain">
                          <Text style={styles.imageTextStyle}>
                            {item.wishCount}
                          </Text>
                        </ImageBackground>
                      ) : (
                        <ImageBackground
                          source={require('../../assets/images/white_heart_icon.png')}
                          style={styles.bgImageStyle}
                          resizeMode="contain">
                          <Text
                            style={[styles.imageTextStyle, {color: '#2E2E2E'}]}>
                            {item.wishCount}
                          </Text>
                        </ImageBackground>
                      )}
                      <Image
                        source={require('../../assets/images/share_icon.png')}
                        style={styles.filterImageStyle}
                        resizeMode="contain"
                      />
                      <ImageBackground
                        source={require('../../assets/images/shopping_card_icon.png')}
                        style={[styles.bgImageStyle]}
                        resizeMode="contain">
                        <Text
                          style={[styles.imageTextStyle, {paddingBottom: 3}]}>  {item.cart}</Text>
                      </ImageBackground>
                      <View style={{marginTop: 5}}>
                        <ImageBackground
                          source={item.roundImage}
                          style={styles.bgImageStyle}
                          resizeMode="contain">
                          <Image
                            source={require('../../assets/images/roundstarImage.png')}
                            style={styles.filterStarImageStyle}
                            resizeMode="contain"
                          />
                        </ImageBackground>
                      </View>
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
            )}
          />
        </View>
      </ScrollView>
      <RBSheet
        ref={refRBSheet}
        customStyles={{
          wrapper: {
            backgroundColor: '#FEF2E6',
            opacity:0.9
          },
          container: commonStyle.sheetBox,
        }}>
        <BottomSheet />
      </RBSheet>
    </View>
  );
};

export default StoreView1;
