import React, { useEffect, useState } from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StatusBar,
  ScrollView,
  FlatList,
  LogBox,
  ImageBackground,
  Dimensions,
} from 'react-native';
import styles from './StoreDetails.Style';
import commonStyle from '../../styles/common.Style';
const StoreDetails = ({ navigation, route }) => {
  const { storeValues, storeValue1 } = route.params;
  let [count, setCount] = useState(1);
  return (
    <View style={styles.container}>
      <StatusBar
        backgroundColor="transparent"
        barStyle="dark-content"
        translucent={true}
      />
      <View>
        <View style={styles.topLineViewStyle}>
          <Image
            source={require('../../assets/images/logo.png')}
            style={styles.logoStyle}
            resizeMode="contain"
          />
        </View>
        <View
          style={{
            ...commonStyle.row,
            ...commonStyle.spaceBtw,
            marginTop: -25,
            marginHorizontal: 20,
          }}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Image
              source={require('../../assets/images/backbutton_icon.png')}
              style={styles.iconStyle}
              resizeMode="contain"
            />
          </TouchableOpacity>
          <View style={{ ...commonStyle.row, marginRight: 0 }}>
            <Image
              source={require('../../assets/images/share.png')}
              style={[styles.iconStyle, { marginRight: 20 }]}
              resizeMode="contain"
            />
            <Image
              source={require('../../assets/images/wish.png')}
              style={styles.iconStyle}
              resizeMode="contain"
            />
          </View>
        </View>
      </View>
      <Text style={styles.headerTextStyle}>Detailansicht</Text>
      <ScrollView>
        <View style={styles.storeView}>
          {storeValue1 ? <ImageBackground
            source={storeValue1.bigImage}
            style={styles.boxBigImageStyle}
            resizeMode="contain">
            <Image
              source={require('../../assets/images/roundstarImage.png')}
              style={styles.filterStarImageStyle}
              resizeMode="contain"
            />
          </ImageBackground> :
            <ImageBackground
              source={require('../../assets/images/DetailsImage1.png')}
              style={styles.boxBigImageStyle}
              resizeMode="contain">
              <Image
                source={require('../../assets/images/roundstarImage.png')}
                style={styles.filterStarImageStyle}
                resizeMode="contain"
              />
            </ImageBackground>
          }
          <View style={{ paddingLeft: 10, marginTop: 10 }}>
            <Text style={styles.storeNameText}>{storeValue1 ? storeValue1.title : "Anna_ix"}</Text>
            <View style={commonStyle.row}>
              <Text style={styles.storeFollowerText}>Follower </Text>
              <Text style={styles.storeFollowerAmount}> {storeValue1 ? storeValue1.followerCount : "69"}k</Text>
            </View>
            <TouchableOpacity onPress={() => navigation.navigate('Feedback')}>
              <Image
                source={require('../../assets/images/start_icon.png')}
                style={styles.starIconStyle}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
        </View>
        {count == '1' && (
          <View style={styles.productView}>
            <Image
              // source={require('../../assets/images/DetailsBigImage1.png')}
              source={storeValues.listImage}
              style={styles.iconBigStyle}
              resizeMode="cover"
            />
          </View>
        )}
        {count == '2' && (
          <View style={styles.productView}>
            <Image
              // source={require('../../assets/images/DetailsBigImage2.png')}
              source={storeValues.listImage}
              style={styles.iconBigStyle}
              resizeMode="stretch"
            />
          </View>
        )}
        {count == '3' && (
          <View style={styles.productView}>
            <Image
              // source={require('../../assets/images/DetailsBigImage3.png')}
              source={storeValues.listImage}
              style={styles.iconBigStyle}
              resizeMode="stretch"
            />
          </View>
        )}
        <ScrollView style={styles.subProductView} horizontal={true}>
          {count != 1 && (
            <View style={[styles.subProductShadow, { marginLeft: 10, marginRight: 10 }]} >
              <TouchableOpacity onPress={() => setCount('1')} >
                <Image
                  // source={require('../../assets/images/SubProductImage3.png')}
                  source={storeValues.listImage}
                  style={styles.iconSubProductStyles1}
                  resizeMode="stretch"
                />
              </TouchableOpacity>
            </View>
          )}
          {count != 2 && (
            <View style={[styles.subProductShadow, { marginLeft: 10, marginRight: 10 }]} >
              <TouchableOpacity onPress={() => setCount('2')} style={styles.subProductShadow} >
                <Image
                  // source={require('../../assets/images/SubProductImage1.png')}
                  source={storeValues.listImage}
                  style={styles.iconSubProductStyle}
                  resizeMode="stretch"
                />
              </TouchableOpacity>
            </View>
          )}
          {count != 3 && (
            <View style={[styles.subProductShadow, { marginLeft: 10, marginRight: 10 }]} >
              <TouchableOpacity onPress={() => setCount('3')} style={styles.subProductShadow} >
                <Image
                  // source={require('../../assets/images/SubProductImage2.png')}
                  source={storeValues.listImage}
                  style={styles.iconSubProductStyle}
                  resizeMode="stretch"
                />
              </TouchableOpacity>
            </View>
          )}
        </ScrollView>
        <View>
          <View style={styles.productDescriptionView}>
            <Text style={styles.storeNameText}>{storeValues.subTitle}</Text>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: -7,
              }}>
              <TouchableOpacity onPress={() => navigation.navigate('Feedback')}>
                <Image
                  source={require('../../assets/images/start_icon.png')}
                  style={styles.starIconStyle}
                  resizeMode="contain"
                />
              </TouchableOpacity>
              <Text style={styles.productFollowerText}> 4.0 (33 Gesehen)</Text>
            </View>
          </View>
          <View style={{ position: 'absolute', bottom: 16, right: 75 }}>
            <Text style={[styles.storeNameText, { fontSize: 18 }]}>{storeValues.price} €</Text>
          </View>
        </View>
        <View style={styles.productDescriptionView}>
          <Text style={styles.storeNameText}>
            {storeValues.title}
          </Text>
          <Text style={styles.storeFollowerText}>Produktdetails</Text>
          <Text style={styles.storeFollowerText}>
            Partiell mit Mesh gefuttert
          </Text>
          <Text style={styles.storeFollowerText}>
            Zwei seitliche Eingriffstachen mit jewwels einem Druckknopf
          </Text>
          <Text style={styles.storeFollowerText}>Elastiche Bundchen</Text>
          <Text style={styles.storeFollowerText}>
            Schlitz mit einem Klettverschluss auf der Ruckseite
          </Text>
          <Text style={styles.storeFollowerText}>
            Reibverschluss entlang der voderseite
          </Text>
          <Text style={styles.storeNameText}>Farbe</Text>
          <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity
              style={[
                styles.productColorView,
                { backgroundColor: '#FEF2E6' },
              ]}></TouchableOpacity>
            <TouchableOpacity
              style={[
                styles.productColorView,
                { backgroundColor: '#08090B', marginLeft: 10 },
              ]}></TouchableOpacity>
            <TouchableOpacity
              style={[
                styles.productColorView,
                { backgroundColor: '#EE3F0A', marginLeft: 10 },
              ]}></TouchableOpacity>
            <TouchableOpacity
              style={[
                styles.productColorView,
                { backgroundColor: '#204D36', marginLeft: 10 },
              ]}></TouchableOpacity>
          </View>
          <Text style={[styles.storeNameText, { marginTop: 10 }]}>Größe</Text>
        </View>
        <TouchableOpacity
          style={styles.KaufenButton}
          onPress={() => navigation.navigate('CheckOut')}>
          <Text style={styles.storeNameText}>Kaufen</Text>
          <Image
            source={require('../../assets/images/shopping_card_icon.png')}
            style={[styles.iconStyle, { position: 'absolute', right: 20 }]}
            resizeMode="contain"
          />
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

export default StoreDetails;
