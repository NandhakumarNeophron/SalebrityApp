import React, { useEffect, useRef } from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StatusBar,
  ScrollView,
  FlatList,
  LogBox,
  ImageBackground,
  Dimensions,
  BackHandler
} from 'react-native';
import styles from './StoreView.Style';
import RBSheet from 'react-native-raw-bottom-sheet';
import BottomSheet from '../Feedback/BottomSheet';
import commonStyle from '../../styles/common.Style';
import { useFocusEffect } from '@react-navigation/native';
const StoreView = ({ navigation, route }) => {
  const { storeValue } = route.params;
  const refRBSheet = useRef();
  useFocusEffect(
    React.useCallback(() => {
      const onBackPress = () => { navigation.navigate('Page3'); return true; };
      BackHandler.addEventListener('hardwareBackPress', onBackPress);
      return () => { BackHandler.removeEventListener('hardwareBackPress', onBackPress); };
    }, []),
  );
  const screenWidth = Math.round(Dimensions.get('window').width);
  const listData = [
    {
      id: '1',
      listImage: require('../../assets/images/list_image1.png'),
      title: 'Public Desire Shoes',
      subTitle: 'Jacke',
      size: '44',
      EUR: '0',
      USA: '0',
      price: '66',
      wishCount: '5',
      isTop: '0',
      cart: '3',
    },
    {
      id: '2',
      listImage: require('../../assets/images/list_image2.png'),
      title: 'Zara Shoes',
      subTitle: 'Schuhe',
      size: '40',
      EUR: '0',
      USA: '0',
      price: '90',
      wishCount: '23',
      isTop: '1',
      cart: '5',
    },
    {
      id: '3',
      listImage: require('../../assets/images/list_image3.png'),
      title: 'Zara Jeans',
      subTitle: 'Schuhe',
      size: '0',
      EUR: '23',
      USA: '2xx24',
      price: '74',
      wishCount: '25',
      isTop: '0',
      cart: '8',
    },
    {
      id: '4',
      listImage: require('../../assets/images/list_image4.png'),
      title: 'Shein Jeans',
      subTitle: 'Kinder Kleidung',
      size: '0',
      EUR: '38',
      USA: '6',
      price: '100',
      wishCount: '39',
      isTop: '1',
      cart: '7',
    },
    {
      id: '5',
      listImage: require('../../assets/images/list_image5.png'),
      title: 'Adika',
      subTitle: 'Tasche',
      size: '0',
      EUR: '38',
      USA: '6',
      price: '56',
      wishCount: '21',
      isTop: '0',
      cart: '1',
    },
  ];
  useEffect(() => {
    LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
  }, []);
  const moveOnStore = async (item) => {
    navigation.navigate('StoreDetails', {
      storeValues: item,
      storeValue1 : storeValue
    });
  }
  return (
    <View style={styles.container}>
      <StatusBar
        backgroundColor="transparent"
        barStyle="dark-content"
        translucent={true}
      />
      <View>
        <View style={commonStyle.topLineViewStyle}>
          <Image
            source={require('../../assets/images/logo.png')}
            style={styles.logoStyle}
            resizeMode="contain"
          />
        </View>
        <View
          style={{
            ...commonStyle.row,
            ...commonStyle.spaceBtw,
            marginTop: -25,
            marginHorizontal: 20,
          }}>
          <TouchableOpacity onPress={() => navigation.navigate('Page3')}>
            <Image
              source={require('../../assets/images/backbutton_icon.png')}
              style={styles.iconStyle}
              resizeMode="contain"
            />
          </TouchableOpacity>
          <Image
            source={require('../../assets/images/dot_icon.png')}
            style={styles.iconStyle}
            resizeMode="contain"
          />
        </View>
      </View>
      <ScrollView>
        <View style={styles.storeView}>
          <Image
            source={storeValue.bigImage}
            style={styles.boxBigImageStyle}
            resizeMode="contain"
          />
          <View style={{ paddingLeft: 10, marginTop: 10 }}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text style={styles.storeNameText}>{storeValue.title}</Text>
              <View style={styles.productRoundView}>
                <Image
                  source={require('../../assets/images/logo.png')}
                  style={styles.boxProductImageStyle}
                  resizeMode="contain"
                />
              </View>
            </View>
            <View style={{ flexDirection: 'row' }}>
              <Text style={styles.storeFollowerText}>Follower</Text>
              <Text
                style={[
                  styles.storeFollowerText1,
                  { opacity: 0.8, paddingLeft: 7 },
                ]}>
                69k
              </Text>
            </View>
            <View style={{ flexDirection: 'row' }}>
              <Text style={styles.storeFollowerText}>Beiträge</Text>
              <Text
                style={[
                  styles.storeFollowerText1,
                  { opacity: 0.8, paddingLeft: 7 },
                ]}>
                432
              </Text>
            </View>
            <TouchableOpacity onPress={() => navigation.navigate('Feedback')}>
              <Image
                source={require('../../assets/images/start_icon.png')}
                style={styles.starIconStyle}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={styles.folgenButtonView}
           /*  onPress={() => navigation.navigate('StoreView1')} */>
            <Text style={styles.buttonText}>Folgen</Text>
          </TouchableOpacity>
          <View
            style={{
              position: 'absolute',
              bottom: 20,
              right: 10,
              flexDirection: 'row',
            }}>
            <Image
              source={require('../../assets/images/share_icon.png')}
              style={[styles.iconStyle, { marginRight: 10 }]}
              resizeMode="contain"
            />
            <ImageBackground
              source={require('../../assets/images/heart_icon.png')}
              style={styles.bgImageStyle}
              resizeMode="contain">
              <Text style={styles.imageTextStyle}> 45 </Text>
            </ImageBackground>
          </View>
        </View>
        <View style={styles.headerView}>
          <TouchableOpacity style={styles.roundView}>
            <Image
              source={require('../../assets/images/filter_icon.png')}
              style={styles.filterImageStle}
              resizeMode="contain"
            />
            <Text style={styles.filterText}>Alle Filter</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.roundView, { position: 'absolute', right: 0 }]}
            onPress={() => refRBSheet.current.open()}>
            <Text style={styles.filterText}>Sortieren </Text>
            <Image
              source={require('../../assets/images/down_arrow_icon.png')}
              style={styles.filterImageStle}
              resizeMode="contain"
            />
          </TouchableOpacity>
        </View>
        <View style={{ marginTop: 20, marginBottom: 20 }}>
          <FlatList
            showsVerticalScrollIndicator={false}
            data={listData}
            removeClippedSubviews={true}
            maxToRenderPerBatch={8}
            windowSize={10}
            initialNumToRender={8}
            keyExtractor={({ id }) => `key-${id}`}
            renderItem={({ item, index }) => (
              <View style={styles.productView}>
                <TouchableOpacity style={styles.productBoxView}  onPress={() =>moveOnStore(item)}>
                  <View style={{}}>
                    <Image
                      source={item.listImage}
                      style={styles.boxImageStyle}
                      resizeMode="cover"
                    />
                  </View>
                  <View
                    style={{
                      // position: 'absolute',
                      marginVertical: 15,
                      justifyContent: 'space-between',
                      flexDirection: 'column',
                    }}>
                    <Text
                      style={[styles.filterText, { width: screenWidth / 2.75 }]}
                      numberOfLines={1}>
                      {item.title}
                    </Text>
                    {item.size > 0 && (
                      <Text style={styles.filterSizeText}>
                        Size {item.size}
                      </Text>
                    )}
                    {item.EUR > 0 && (
                      <Text style={styles.filterSizeText}>EUR {item.EUR}</Text>
                    )}
                    {item.USA > 0 && (
                      <Text style={styles.filterSizeText}>USA {item.USA}</Text>
                    )}
                    <Text style={styles.filterText}>{item.price} €</Text>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'center',
                    }}>
                    <View
                      style={{
                        marginLeft: 30,
                        marginRight: 15,
                        justifyContent: 'center',
                      }}>
                      {item.isTop == 1 ? (
                        <ImageBackground
                          source={require('../../assets/images/heart_icon.png')}
                          style={styles.bgImageStyle}
                          resizeMode="contain">
                          <Text style={styles.imageTextStyle}>
                            {item.wishCount}{' '}
                          </Text>
                        </ImageBackground>
                      ) : (
                        <ImageBackground
                          source={require('../../assets/images/white_heart_icon.png')}
                          style={styles.bgImageStyle}
                          resizeMode="contain">
                          <Text
                            style={[styles.imageTextStyle, { color: '#000000' }]}>
                            {item.wishCount}
                          </Text>
                        </ImageBackground>
                      )}

                      <Image
                        source={require('../../assets/images/share_icon.png')}
                        style={styles.filterImageStyle}
                        resizeMode="contain"
                      />
                      <ImageBackground
                        source={require('../../assets/images/shopping_card_icon.png')}
                        style={styles.bgImageStyle}
                        resizeMode="contain">
                        <Text
                          style={[styles.imageTextStyle, { paddingBottom: 2 }]}>  {item.cart}</Text>
                      </ImageBackground>
                    </View>
                  </View>
                </TouchableOpacity>
                <View style={{ marginTop: 15 }}></View>
              </View>
            )}
          />
        </View>
      </ScrollView>
      <RBSheet
        ref={refRBSheet}
        customStyles={{
          wrapper: {
            // backgroundColor: 'transparent',
            backgroundColor: '#FEF2E6',
            opacity: 0.9
          },
          container: styles.sheetBox,
        }}>
        <BottomSheet />
      </RBSheet>
    </View>
  );
};

export default StoreView;
