import { StyleSheet, Dimensions } from 'react-native';
import { fonts, colors } from '../../styles/theme';
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white_color,
  },
  topLineViewStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 55,
    // marginLeft: 20,
  },
  iconStyle: {
    width: 24,
    height: 24,
  },
  logoViewStyle: {
    marginLeft: screenWidth / 3.75,
  },
  logoStyle: {
    width: 86,
    height: 86,
    marginTop: -60,
  },
  headerTextStyle: {
    fontFamily: fonts.semibold_font,
    fontSize: 16,
    color: colors.font_color,
    textAlign: 'center',
    marginTop: 15,
  },
  boxBigImageStyle: {
    width: screenWidth / 3.5,
    height: screenHeight / 6.5,
  },
  storeNameText: {
    fontFamily: fonts.semibold_font,
    fontSize: 16,
    color: colors.font_color,
    lineHeight: 36,
  },
  storeFollowerText: {
    fontFamily: fonts.medium_font,
    fontWeight: '500',
    fontSize: 10,
    color: colors.font_color,
    lineHeight: 22,
  },
  storeFollowerAmount: {
    fontFamily: fonts.regular_font,
    fontWeight: '400',
    fontSize: 10,
    color: colors.font_color,
    opacity: 0.8,
    lineHeight: 22,
  },
  starIconStyle: {
    width: 60,
    height: 30,
  },
  storeView: {
    margin: 20,
    flexDirection: 'row',
  },
  filterStarImageStyle: {
    width: 24,
    height: 24,
    position: 'absolute',
    bottom: 0,
    right: -10,
  },
  iconBigStyle: {
    width: screenWidth - 40,
    height: 300,
    borderRadius:16,
    overflow:'hidden'
  },
  productView: {
    alignSelf: 'center',
    backgroundColor: '#EAECEE',
    justifyContext: 'center',
    alignItems: 'center',
    paddingBottom: 5,
    padding: 2,
    borderRadius: 20,
    elevation: 1
  },
  subProductView: {
    flex: 1,
    paddingLeft: 10,
    paddingRight: 20,
    marginTop: 15,
    flexDirection: 'row',
  },
  iconSubProductStyle: {
    width: (screenWidth - 50) / 2,
    height: (screenWidth - 50) / 2,
    borderRadius:16,
    overflow:'hidden'
  },
  iconSubProductStyles: {
    width: (screenWidth - 50) / 2.15,
    height: (screenWidth - 50) / 1.9,
  },
  iconSubProductStyles1: {
    width: (screenWidth - 50) / 1.9,
    height: (screenWidth - 50) / 1.9,
  },
  productDescriptionView: {
    // marginTop: 8,
    marginLeft: 20,
  },
  productFollowerText: {
    fontFamily: fonts.regular_font,
    fontSize: 12,
    color: colors.font_color,
  },
  productColorView: {
    width: 32,
    height: 32,
    borderRadius: 100,
    borderWidth: 0.5,
  },
  KaufenButton: {
    width: screenWidth - 50,
    height: screenHeight / 14,
    backgroundColor: colors.primary_color,
    borderWidth: 0.5,
    alignSelf: 'center',
    borderRadius: 16,
    marginTop: 20,
    marginBottom: 20,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  shadowView: {
    shadowColor: '#000030',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    elevation: 2,
    borderRadius: 12,
    // marginLeft: 12,
  },
  subProductShadow: {
    flex: 1,
    backgroundColor: '#EAECEE',
    paddingBottom: 3,
    padding: 2,
    borderRadius: 16,
  }
});
