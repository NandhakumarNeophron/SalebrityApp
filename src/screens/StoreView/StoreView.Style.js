import {StyleSheet, Dimensions} from 'react-native';
import {fonts, colors} from '../../styles/theme';
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white_color,
  },
  topLineViewStyle: {
    flexDirection: 'row',
    marginTop: 50,
    marginLeft: 20,
  },
  iconStyle: {
    width: 24,
    height: 24,
  },
  logoViewStyle: {
    marginLeft: screenWidth / 3.75,
  },
  logoStyle: {
    width: 86,
    height: 86,
    marginTop: -60,
  },
  storeView: {
    margin: 20,
    flexDirection: 'row',
  },
  boxBigImageStyle: {
    width: screenWidth / 3.5,
    height: screenHeight / 6.5,
  },
  storeNameText: {
    fontFamily: fonts.semibold_font,
    fontSize: 16,
    color: colors.font_color,
  },
  storeFollowerText: {
    fontFamily: fonts.medium_font,
    fontSize: 14,
    color: colors.font_color,
    lineHeight: 22,
  },
  storeFollowerText1: {
    fontFamily: fonts.regular_font,
    fontSize: 14,
    color: colors.font_color,
    lineHeight: 22,
  },
  starIconStyle: {
    width: 60,
    height: 30,
  },
  folgenButtonView: {
    width: screenWidth / 3.7,
    height: screenHeight / 22,
    backgroundColor: '#00A3FF',
    borderRadius: 8,
    position: 'absolute',
    right: 0,
    top: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    fontFamily: fonts.semibold_font,
    fontSize: 12,
    color: colors.white_color,
  },
  roundView: {
    width: screenWidth / 2.75,
    height: screenHeight / 22,
    backgroundColor: colors.white_color,
    borderWidth: 0.5,
    borderColor: '#969696',
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  headerView: {
    width: screenWidth - 40,
    alignSelf: 'center',
    flexDirection: 'row',
  },
  filterText: {
    fontFamily: fonts.semibold_font,
    fontSize: 14,
    color: colors.font_color,
    paddingLeft: 15,
  },
  filterImageStle: {
    width: 16,
    height: 16,
  },
  productView: {
    alignSelf: 'center',
  },
  productBoxView: {
    width: screenWidth - 40,
    height: screenHeight / 5.5,
    backgroundColor: colors.white_color,
    marginTop: 10,
    borderRadius: 16,
    borderWidth: 0.5,
    borderColor: colors.font_color,
    marginBottom: 2.5,
    paddingVertical: 8,
    // justifyContent: 'center',
    alignSelf: 'center',
    flexDirection: 'row',
    elevation: 10,
    borderRadius: 16,
    borderWidth: 0.5,
    borderColor: '#cbcdce',
  },
  boxImageStyle: {
    width: screenWidth / 2.95,
    height: screenHeight / 6.5,
    borderRadius: 16,
    overflow: 'hidden',
    marginLeft: 10,
  },
  productRoundView: {
    width: 16,
    height: 16,
    borderRadius: 100,
    backgroundColor: colors.primary_color,
    marginLeft: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  boxProductImageStyle: {
    width: 14,
    height: 14,
  },
  filterImageStyle: {
    width: 20,
    height: 20,
    marginBottom: 15,
    marginTop: 15,
  },
  bgImageStyle: {
    width: 26,
    height: 26,
    justifyContent: 'center',
  },
  imageTextStyle: {
    fontFamily: fonts.bold_font,
    fontSize: 8,
    color: colors.white_color,
    textAlign: 'center',
  },
  filterSizeText: {
    fontFamily: fonts.regular_font,
    fontSize: 12,
    color: colors.font_color,
    paddingLeft: 15,
    lineHeight: 22,
  },
  sheetBox: {
    backgroundColor: colors.primary_color,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    height: screenHeight / 2.2,
  },
});
