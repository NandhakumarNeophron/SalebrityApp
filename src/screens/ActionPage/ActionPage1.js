import React, { useEffect } from 'react';
import { View, Text, Image, TouchableOpacity, StatusBar, ScrollView, TextInput, FlatList, LogBox, ImageBackground, Dimensions, } from 'react-native';
import styles from './ActionPage1.Style';
import commonStyle from '../../styles/common.Style';
import { colors } from '../../styles/theme';
const ActionPage1 = ({ navigation }) => {
  const screenWidth = Math.round(Dimensions.get('window').width);
  const listData = [
    {
      id: '1',
      listImage: require('../../assets/images/list_image6.png'),
      roundImage: require('../../assets/images/round_image1.png'),
      title: 'Burberry Jacke',
      subTitle: 'Jacke',
      size: 'XS',
      EUR: '0',
      USA: '0',
      price: '639',
      wishCount: '5',
      isTop: '0',
      cart: '3',
    },
    {
      id: '2',
      listImage: require('../../assets/images/list_image7.png'),
      roundImage: require('../../assets/images/round_image2.png'),
      title: 'Nike Air Force',
      subTitle: 'Schuhe',
      size: '40',
      EUR: '0',
      USA: '0',
      price: '90',
      wishCount: '23',
      isTop: '1',
      cart: '5',
    },
    {
      id: '3',
      listImage: require('../../assets/images/list_image8.png'),
      roundImage: require('../../assets/images/round_image3.png'),
      title: 'Nike Sneaker',
      subTitle: 'Schuhe',
      size: '43',
      EUR: '0',
      USA: '2xx24',
      price: '56',
      wishCount: '21',
      isTop: '0',
      cart: '1',
    },
    {
      id: '4',
      listImage: require('../../assets/images/list_image9.png'),
      roundImage: require('../../assets/images/round_image4.png'),
      title: 'Baby Winteroverall',
      subTitle: 'Kinder Kleidung',
      size: '',
      EUR: '98',
      USA: '',
      price: '100',
      wishCount: '39',
      isTop: '1',
      cart: '7',
    },
    {
      id: '5',
      listImage: require('../../assets/images/list_image10.png'),
      roundImage: require('../../assets/images/round_image1.png'),
      title: 'Tasche',
      subTitle: 'Tasche',
      size: 'XS',
      EUR: '',
      USA: '',
      price: '56',
      wishCount: '25',
      isTop: '0',
      cart: '8',
    },
  ];
  useEffect(() => {
    LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
  }, []);
  const moveOnStore = async (item) => {
    navigation.navigate('StoreDetails', {
      storeValues: item,
    });
 };
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="transparent" barStyle="dark-content" translucent={true} hidden={false}/>
      <View style={styles.topViewStyle}>
        <View style={styles.topLineViewStyle}>
          <Image
            source={require('../../assets/images/logo.png')}
            style={styles.logoStyle}
            resizeMode="contain"
          />
        </View>
        <View
          style={{
            ...commonStyle.row,
            ...commonStyle.spaceBtw,
            marginTop: -25,
          }}>
          <Image
            source={require('../../assets/images/menu_icon.png')}
            style={[styles.iconStyle, , { marginLeft: 20 }]}
            resizeMode="contain"
          />
          <View style={{ ...commonStyle.row, marginRight: 10 }}>
            <Image
              source={require('../../assets/images/user_icon.png')}
              style={[styles.iconStyle, { marginRight: 20 }]}
              resizeMode="contain"
            />
            <TouchableOpacity onPress={() => navigation.navigate('CheckOut')}>
              <Image
                source={require('../../assets/images/shopping_card_icon.png')}
                style={styles.iconStyle}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
        </View>
        <View>
          <TouchableOpacity style={styles.searchViewStyle}>
            <Image
              source={require('../../assets/images/search_icon.png')}
              style={{ width: 20, height: 20 }}
              resizeMode="contain"
            />
            <TextInput
              style={styles.searchTextStyle}
              underlineColorAndroid="transparent"
              placeholder="Suchen Products..."
              placeholderTextColor="#2e2e2e66"
              numberOfLines={1}
            />
            <TouchableOpacity
              style={styles.filterview}
              onPress={() => navigation.navigate('FilterView')}>
              <Image
                source={require('../../assets/images/filter_icon.png')}
                style={styles.iconStyle}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </TouchableOpacity>
        </View>
      </View>
      <ScrollView style={{ marginTop: -20 }} showsVerticalScrollIndicator={false}  >
        <View style={styles.centerViewStyle}>
          <ImageBackground
            source={require('../../assets/images/banner_box.png')}
            style={styles.boxImageStyles}
            resizeMode="stretch">
            <Text style={styles.boxheadertext}>Aufmerksamkeit</Text>
            <Text style={styles.boxsubheadertext}>
              H&M, Zara, Zalando setzen sich {'\n'}fur Recycling ein!
            </Text>
            <View style={{ flexDirection: 'row' }}>
              <Image
                source={require('../../assets/images/line_icon.png')}
                style={styles.iconLineStyle}
                resizeMode="contain"
              />
              <Image
                source={require('../../assets/images/line_icon.png')}
                style={styles.iconLineStyle}
                resizeMode="contain"
              />
              <Image
                source={require('../../assets/images/line_icon.png')}
                style={styles.iconLineStyle}
                resizeMode="contain"
              />
              <Image
                source={require('../../assets/images/line_icon.png')}
                style={styles.iconLineStyle}
                resizeMode="contain"
              />
            </View>
          </ImageBackground>
          <View style={styles.productListView}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text style={styles.headerText}>Auktion</Text>
              <TouchableOpacity style={{ position: 'absolute', right: 20 }} onPress={() => navigation.navigate('StoreView1')}>
                <Text style={styles.headerRightText}>Alle</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View>
            <View style={styles.colorBoxView}>
              <Text style={styles.colorBoxText}>H&M Sale</Text>
            </View>
            <View
              style={{
                width: 2,
                height: 30,
                backgroundColor: '#FF6536',
                alignSelf: 'center',
              }}
            />
            <ImageBackground
              source={require('../../assets/images/adImage1.png')}
              style={styles.boxBigImageStyle}
              resizeMode="contain">
              <View
                style={{ marginTop: 30, flexDirection: 'row', marginLeft: 20 }}>
                <TouchableOpacity>
                  <Image
                    source={require('../../assets/images/orange_share.png')}
                    style={styles.shareImageStyle}
                    resizeMode="contain"
                  />
                </TouchableOpacity>
                <TouchableOpacity style={{ position: 'absolute', right: 20 }}>
                  <Image
                    source={require('../../assets/images/orange_heart.png')}
                    style={styles.shareImageStyle}
                    resizeMode="contain"
                  />
                </TouchableOpacity>
              </View>
            </ImageBackground>
          </View>
          <Text style={styles.adHeaderText}>Alle Hot SALE von H&MMjetzt!!</Text>
          <View>
            <View
              style={[
                styles.colorBoxView,
                { borderColor: '#EB00C3', marginTop: 30 },
              ]}>
              <Text style={[styles.colorBoxText, { color: '#EB00C3' }]}>
                Zara Sale
              </Text>
            </View>
            <View
              style={{
                width: 2,
                height: 26,
                backgroundColor: '#EB00C3',
                alignSelf: 'center',
              }}
            />
            <ImageBackground
              source={require('../../assets/images/adImage2.png')}
              style={{ ...styles.boxBigImageStyle, marginTop: -24 }}
              resizeMode="contain">
              <View
                style={{ marginTop: 35, flexDirection: 'row', marginLeft: 20 }}>
                <TouchableOpacity>
                  <Image
                    source={require('../../assets/images/pink_share.png')}
                    style={styles.shareImageStyle}
                    resizeMode="contain"
                  />
                </TouchableOpacity>
                <TouchableOpacity style={{ position: 'absolute', right: 20 }}>
                  <Image
                    source={require('../../assets/images/pink_heart.png')}
                    style={styles.shareImageStyle}
                    resizeMode="contain"
                  />
                </TouchableOpacity>
              </View>
            </ImageBackground>
          </View>
          <Text
            style={[styles.adHeaderText, { color: '#EB00C3', paddingTop: 2.5 }]}>
            Alle Hot SALE von H6M Jetzt!!
          </Text>
          <View
            style={{ flexDirection: 'row', marginTop: 25, alignSelf: 'center' }}>
            <TouchableOpacity
              style={[styles.horizontalbuttonStyle, { marginLeft: -5 }]}>
              <Text style={styles.horizontalbuttonText}>Zara</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.horizontalbuttonStyle}>
              <Text style={styles.horizontalbuttonText}>Bershka</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.horizontalbuttonStyle}>
              <Text style={styles.horizontalbuttonText}>Chanel</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.horizontalbuttonStyle}>
              <Text style={styles.horizontalbuttonText}>Prada</Text>
            </TouchableOpacity>
          </View>
          <View
            style={{ flexDirection: 'row', marginTop: 10, alignSelf: 'center' }}>
            <TouchableOpacity
              style={[styles.horizontalbuttonStyle, { marginLeft: -5 }]}>
              <Text style={styles.horizontalbuttonText}>H&M</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.horizontalbuttonStyle}>
              <Text style={styles.horizontalbuttonText}>Prada</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.horizontalbuttonStyle}>
              <Text style={styles.horizontalbuttonText}>Apple</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.horizontalbuttonStyle}>
              <Text style={styles.horizontalbuttonText}>Apple</Text>
            </TouchableOpacity>
          </View>
          <FlatList
            showsVerticalScrollIndicator={false}
            data={listData}
            removeClippedSubviews={true}
            maxToRenderPerBatch={8}
            windowSize={10}
            initialNumToRender={8}
            keyExtractor={({ id }) => `key-${id}`}
            renderItem={({ item, index }) => (
              <View style={styles.productView}>
                <TouchableOpacity
                  style={styles.productBoxView}
                  onPress={() =>moveOnStore(item)}>
                  <View style={{ position: 'absolute', left: -2 }}>
                    <Image
                      source={item.listImage}
                      style={styles.boxImageStyle}
                      resizeMode="cover"
                    />
                  </View>
                  <View
                    style={{ position: 'absolute', left: screenWidth / 2.85 }}>
                    <Text
                      style={[styles.filterText, { width: screenWidth / 3 }]}
                      numberOfLines={1}>
                      {item.title}
                    </Text>
                    {item.size != '' && (
                      <Text style={styles.filterSizeText}>
                        Size: {item.size}
                      </Text>
                    )}
                    {item.EUR > 0 && (
                      <Text style={styles.filterSizeText}>EUR: {item.EUR}</Text>
                    )}
                    {item.USA > 0 && (
                      <Text style={styles.filterSizeText}>USA: {item.USA}</Text>
                    )}
                    <Text style={styles.filterText}>{item.price} €</Text>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      position: 'absolute',
                      right: 0,
                      justifyContent: 'center',
                    }}>
                    <View
                      style={{
                        marginLeft: 30,
                        marginRight: 15,
                        justifyContent: 'center',
                      }}>
                      {item.isTop == 1 ? (
                        <ImageBackground
                          source={require('../../assets/images/heart_icon.png')}
                          style={styles.bgImageStyle}
                          resizeMode="contain">
                          <Text style={styles.imageTextStyle}>{item.wishCount}</Text>
                        </ImageBackground>
                      ) : (
                        <ImageBackground
                          source={require('../../assets/images/white_heart_icon.png')}
                          style={styles.bgImageStyle}
                          resizeMode="contain">
                          <Text
                            style={{
                              ...styles.imageTextStyle,
                              color: colors.black_color,
                            }}>
                            {item.wishCount}
                          </Text>
                        </ImageBackground>
                      )}
                      <Image
                        source={require('../../assets/images/share_icon.png')}
                        style={styles.filterImageStyle}
                        resizeMode="contain"
                      />
                      <ImageBackground
                        source={require('../../assets/images/shopping_card_icon.png')}
                        style={styles.bgImageStyle}
                        resizeMode="contain">
                        <Text
                          style={[styles.imageTextStyle,{paddingBottom:2}]}>  {item.cart}</Text>
                      </ImageBackground>
                      <View style={{ marginTop: 5 }}>
                        <ImageBackground
                          source={item.roundImage}
                          style={styles.bgImageStyle}
                          resizeMode="contain">
                          <Image
                            source={require('../../assets/images/roundstarImage.png')}
                            style={styles.filterStarImageStyle}
                            resizeMode="contain"
                          />
                        </ImageBackground>
                      </View>
                    </View>
                  </View>
                </TouchableOpacity>
                <View style={{ marginTop: 20 }}></View>
              </View>
            )}
          />
        </View>
      </ScrollView>
    </View>
  );
};

export default ActionPage1;
