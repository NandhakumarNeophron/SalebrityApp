import React, { useEffect, useRef, useState } from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StatusBar,
  ScrollView,
  TextInput,
  FlatList,
  LogBox,
  ImageBackground,
  Dimensions,
} from 'react-native';
import styles from './ActionPage2.Style';
import commonStyle from '../../styles/common.Style';
import { colors } from '../../styles/theme';
const ActionPage2 = ({ navigation }) => {
  const screenWidth = Math.round(Dimensions.get('window').width);
  const [hour, setHour] = useState(3);
  const [minutes, setMinutes] = useState(9);
  const [seconds, setSeconds] = useState(34);
  useEffect(() => {
    const interval = setInterval(() => {
      if (seconds > 0) {
        setSeconds(seconds - 1);
      }
      if (seconds === 0) {
        if (minutes === 0) {
          clearInterval(interval);
        }
        else if (minutes === 0) {
          setHour(hour - 1);
        } else {
          setSeconds(59);
          setMinutes(minutes - 1);
        }
      }
    }, 1000);
    return () => {
      clearInterval(interval);
    };
  });
  const listData = [
    {
      id: '1',
      listImage: require('../../assets/images/product1.png'),
      title: 'Endet Freitag 14:00',
      date: '13-14. Marz 2023',
      subTitle: 'Chanel kleid',
      size: 'XS',
      Wert: '400',
      Letztes_Gebot: '600',
    },
    {
      id: '2',
      listImage: require('../../assets/images/product2.png'),
      title: 'Endet Sonntg 10:00',
      date: '13-24. Marz 2023',
      subTitle: 'Zara Anzug',
      size: 'XS',
      Wert: '400',
      Letztes_Gebot: '600',
    },
    {
      id: '3',
      listImage: require('../../assets/images/product3.png'),
      title: 'Endet Sonntg 10:00',
      date: '13-24. Marz 2023',
      subTitle: 'Zara Anzug',
      size: 'XS',
      Wert: '400',
      Letztes_Gebot: '600',
    },
  ];
  useEffect(() => {
    LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
  }, []);
  return (
    <View style={styles.container}>
      <StatusBar
        backgroundColor="transparent"
        barStyle="dark-content"
        translucent={true}
      />
      <View style={styles.topViewStyle}>
        <View style={styles.topLineViewStyle}>
          <Image
            source={require('../../assets/images/logo.png')}
            style={styles.logoStyle}
            resizeMode="contain"
          />
        </View>
        <View
          style={{
            ...commonStyle.row,
            ...commonStyle.spaceBtw,
            marginTop: -25,
          }}>
          <Image
            source={require('../../assets/images/menu_icon.png')}
            style={[styles.iconStyle, , { marginLeft: 20 }]}
            resizeMode="contain"
          />
          <View style={{ ...commonStyle.row, marginRight: 10 }}>
            <Image
              source={require('../../assets/images/user_icon.png')}
              style={[styles.iconStyle, { marginRight: 20 }]}
              resizeMode="contain"
            />
            <TouchableOpacity onPress={() => navigation.navigate('CheckOut')}>
              <Image
                source={require('../../assets/images/shopping_card_icon.png')}
                style={styles.iconStyle}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </View>
        </View>
        <View>
          <TouchableOpacity style={styles.searchViewStyle}>
            <Image
              source={require('../../assets/images/search_icon.png')}
              style={{ width: 20, height: 20 }}
              resizeMode="contain"
            />
            <TextInput
              style={styles.searchTextStyle}
              underlineColorAndroid="transparent"
              placeholder="Suchen Products..."
              placeholderTextColor="#2e2e2e66"
              numberOfLines={1}
            />
            <TouchableOpacity
              style={styles.filterview}
              onPress={() => navigation.navigate('FilterView')}>
              <Image
                source={require('../../assets/images/filter_icon.png')}
                style={styles.iconStyle}
                resizeMode="contain"
              />
            </TouchableOpacity>
          </TouchableOpacity>
        </View>
      </View>
      <ScrollView
        style={{ flex: 1, marginTop: -20 }}
        showsVerticalScrollIndicator={false}
        nestedScrollEnabled={true}>
        <View style={styles.centerViewStyle}>
          <ImageBackground
            source={require('../../assets/images/banner_box.png')}
            style={styles.boxImageStyles}
            resizeMode="stretch">
            <Text style={styles.boxheadertext}>Aufmerksamkeit</Text>
            <Text style={styles.boxsubheadertext}>
              H&M, Zara, Zalando setzen sich {'\n'}fur Recycling ein!
            </Text>
            <View style={{ flexDirection: 'row' }}>
              <Image
                source={require('../../assets/images/line_icon.png')}
                style={styles.iconLineStyle}
                resizeMode="contain"
              />
              <Image
                source={require('../../assets/images/line_icon.png')}
                style={styles.iconLineStyle}
                resizeMode="contain"
              />
              <Image
                source={require('../../assets/images/line_icon.png')}
                style={styles.iconLineStyle}
                resizeMode="contain"
              />
              <Image
                source={require('../../assets/images/line_icon.png')}
                style={styles.iconLineStyle}
                resizeMode="contain"
              />
            </View>
          </ImageBackground>
          <View style={styles.productListView}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Text style={styles.headerText}>Auktion</Text>
              <TouchableOpacity style={{ position: 'absolute', right: 20 }} onPress={() => navigation.navigate('StoreView1')}>
                <Text style={styles.headerRightText}>Alle</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View>
            <View style={styles.colorBoxView}>
              <Text style={styles.colorBoxText}>Chanel Auktionen</Text>
            </View>
            <View
              style={{
                width: 2,
                height: 26,
                backgroundColor: colors.font_color,
                alignSelf: 'center',
              }}
            />
            <ImageBackground
              source={require('../../assets/images/adImage3.png')}
              style={{ ...styles.boxBigImageStyle, marginTop: -22 }}
              resizeMode="contain">
              <ImageBackground
                source={require('../../assets/images/adImage5.png')}
                style={styles.boxBigImageStyle1}
                resizeMode="contain">
                <View
                  style={{
                    marginTop: 30,
                    flexDirection: 'row',
                    marginLeft: 20,
                    flex: 1,
                  }}>
                  <TouchableOpacity>
                    <Image
                      source={require('../../assets/images/white_share.png')}
                      style={styles.shareImageStyle}
                      resizeMode="contain"
                    />
                  </TouchableOpacity>
                  <TouchableOpacity style={{ position: 'absolute', right: 20 }}>
                    <Image
                      source={require('../../assets/images/white_heart_icon.png')}
                      style={styles.shareImageStyle}
                      resizeMode="contain"
                    />
                  </TouchableOpacity>
                  <View style={{ marginTop: 50 }}>
                    <Text style={styles.counterText}>
                      {hour < 10 ? `0${hour}` : hour}:{''}
                      {minutes < 10 ? `0${minutes}` : minutes}:{''}
                      {seconds < 10 ? `0${seconds}` : seconds}
                      {''}
                    </Text>
                  </View>
                </View>
              </ImageBackground>
            </ImageBackground>
          </View>
          <Text style={styles.adHeaderText}>
            Alle Auktionen von Chanel heir
          </Text>
          <View>
            <View
              style={[
                styles.colorBoxView,
                { borderColor: '#EB00C3', marginTop: 30 },
              ]}>
              <Text style={[styles.colorBoxText, { color: '#EB00C3' }]}>
                Zara Sale
              </Text>
            </View>
            <View
              style={{
                width: 2,
                height: 26,
                backgroundColor: '#EB00C3',
                alignSelf: 'center',
              }}
            />
            <ImageBackground
              source={require('../../assets/images/adImage2.png')}
              style={{ ...styles.boxBigImageStyle, marginTop: -24 }}
              resizeMode="contain">
              <ImageBackground
                source={require('../../assets/images/adImage4.png')}
                style={[styles.boxBigImageStyle]}
                resizeMode="contain">
                <View
                  style={{ marginTop: 35, flexDirection: 'row', marginLeft: 20 }}>
                  <TouchableOpacity>
                    <Image
                      source={require('../../assets/images/pink_share.png')}
                      style={styles.shareImageStyle}
                      resizeMode="contain"
                    />
                  </TouchableOpacity>
                  <TouchableOpacity style={{ position: 'absolute', right: 20 }}>
                    <Image
                      source={require('../../assets/images/pink_heart.png')}
                      style={styles.shareImageStyle}
                      resizeMode="contain"
                    />
                  </TouchableOpacity>
                  <View style={{ marginTop: 50 }}>
                    <Text style={styles.counterText}>
                      {hour < 10 ? `0${hour}` : hour}:{''}
                      {minutes < 10 ? `0${minutes}` : minutes}:{''}
                      {seconds < 10 ? `0${seconds}` : seconds}
                      {''}
                    </Text>
                  </View>
                </View>
              </ImageBackground>
            </ImageBackground>
          </View>
          <Text
            style={[styles.adHeaderText, { color: '#EB00C3', paddingTop: 2.5 }]}>
            Alle Auktionen von Zara heir
          </Text>
          <View>
            <View
              style={{
                flexDirection: 'row',
                marginTop: 25,
                alignSelf: 'center',
              }}>
              <TouchableOpacity
                style={[styles.horizontalbuttonStyle, { marginLeft: -5 }]}>
                <Text style={styles.horizontalbuttonText}>Zara</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.horizontalbuttonStyle}>
                <Text style={styles.horizontalbuttonText}>Bershka</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.horizontalbuttonStyle}>
                <Text style={styles.horizontalbuttonText}>Chanel</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.horizontalbuttonStyle}>
                <Text style={styles.horizontalbuttonText}>Prada</Text>
              </TouchableOpacity>
            </View>
            <View
              style={{
                flexDirection: 'row',
                marginTop: 10,
                alignSelf: 'center',
              }}>
              <TouchableOpacity
                style={[styles.horizontalbuttonStyle, { marginLeft: -5 }]}>
                <Text style={styles.horizontalbuttonText}>H&M</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.horizontalbuttonStyle}>
                <Text style={styles.horizontalbuttonText}>Prada</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.horizontalbuttonStyle}>
                <Text style={styles.horizontalbuttonText}>Apple</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.horizontalbuttonStyle}>
                <Text style={styles.horizontalbuttonText}>Apple</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={{ marginBottom: 10, marginTop: 20 }}>
            <FlatList
              showsVerticalScrollIndicator={false}
              data={listData}
              removeClippedSubviews={true}
              maxToRenderPerBatch={8}
              windowSize={10}
              initialNumToRender={8}
              keyExtractor={({ id }) => `key-${id}`}
              renderItem={({ item, index }) => (
                <View style={styles.productView}>
                  <View>
                    <Text style={styles.centerTextStyle}>{item.title}</Text>
                    <Text style={styles.centerdateTextStyle}>{item.date}</Text>
                    <TouchableOpacity style={styles.productBoxView}>
                      <View style={{ position: 'absolute', left: -2 }}>
                        <Image
                          source={item.listImage}
                          style={styles.boxImageStyle}
                          resizeMode="cover"
                        />
                      </View>
                      <View
                        style={{
                          position: 'absolute',
                          left: screenWidth / 2.85,
                        }}>
                        <View
                          style={{
                            flexDirection: 'row',
                            position: 'absolute',
                            right: 0,
                          }}>
                          <Image
                            source={require('../../assets/images/share_icon.png')}
                            style={[styles.shareImageStyle, { marginRight: 15 }]}
                            resizeMode="contain"
                          />
                          <Image
                            source={require('../../assets/images/heart_icon.png')}
                            style={styles.shareImageStyle}
                            resizeMode="contain"
                          />
                        </View>
                        <Text style={styles.filterText}>{item.subTitle}</Text>
                        {item.size != '' && (
                          <Text style={styles.filterSizeText}>
                            Size: {item.size}
                          </Text>
                        )}
                        {item.Wert > 0 && (
                          <Text style={styles.filterSizeText}>
                            Wert: {item.Wert}
                          </Text>
                        )}
                        {item.Letztes_Gebot > 0 && (
                          <Text style={styles.filterSizeText}>
                            Letztes Gebot: {item.Letztes_Gebot}
                          </Text>
                        )}
                        <View style={{ flexDirection: 'row', marginTop: 15 }}>
                          <TouchableOpacity style={styles.buttonView}>
                            <Text style={styles.buttonText}>Betrag</Text>
                          </TouchableOpacity>
                          <TouchableOpacity
                            style={[
                              styles.buttonView,
                              { backgroundColor: '#FF6536' },
                            ]}>
                            <Text
                              style={[styles.buttonText, { color: '#FFFFFF' }]}>
                              Bieten
                            </Text>
                          </TouchableOpacity>
                        </View>
                      </View>
                    </TouchableOpacity>
                  </View>
                  <View style={{ marginTop: 20 }}></View>
                </View>
              )}
            />
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default ActionPage2;
