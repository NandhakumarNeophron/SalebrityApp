import { StyleSheet, Dimensions } from 'react-native';
import { fonts, colors } from '../../styles/theme';
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white_color,
  },
  topViewStyle: {
    width: screenWidth,
    height: screenHeight / 4,
    backgroundColor: colors.primary_color,
  },
  topLineViewStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 55,
    // marginLeft: 20,
  },
  iconStyle: {
    width: 24,
    height: 24,
  },
  logoViewStyle: {
    marginLeft: screenWidth / 3.75,
  },
  logoStyle: {
    width: 86,
    height: 86,
    marginTop: -60,
  },
  searchViewStyle: {
    flexDirection: 'row',
    width: screenWidth - 40,
    height: screenHeight / 16,
    alignSelf: 'center',
    backgroundColor: colors.white_color,
    marginTop: 20,
    borderRadius: 16,
    borderWidth: 1,
    borderColor: '#969696',
    paddingHorizontal: 15,
    alignItems: 'center',
    elevation: 10,
    shadowColor: '#051013',
    shadowOpacity: 10,
  },
  searchTextStyle: {
    width: screenWidth - 120,
    fontFamily: fonts.medium_font,
    fontSize: 16,
    textAlign: 'center',
  },
  centerViewStyle: {
    flex: 1,
    backgroundColor: colors.white_color,
    borderTopLeftRadius: 24,
    borderTopRightRadius: 24,
    marginBottom: 20,
  },
  boxViewStyle: {
    width: screenWidth - 40,
    height: screenHeight / 4.5,
    backgroundColor: colors.primary_color,
    alignSelf: 'center',
    marginTop: 20,
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
    borderBottomLeftRadius: 16,
    borderBottomEndRadius: 56,
    borderWidth: 0.5,
    shadowColor: '#2E2E2E',
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 1,
    shadowRadius: 0,
    elevation: 10,
  },
  boxheadertext: {
    fontFamily: fonts.medium_font,
    fontSize: 20,
    color: colors.font_color,
    padding: 20,
  },
  boxsubheadertext: {
    fontFamily: fonts.regular_font,
    fontSize: 16,
    paddingLeft: 20,
    lineHeight: 20,
    color: colors.font_color,
    opacity: 0.8,
  },
  iconLineStyle: {
    width: screenWidth / 7,
    height: 10,
    marginLeft: 20,
    marginTop: 20,
  },
  productListView: {
    marginTop: 20,
    marginLeft: 20,
    marginBottom: 15,
  },
  headerText: {
    fontFamily: fonts.semibold_font,
    fontSize: 20,
    color: colors.font_color,
    fontWeight: '600',
  },
  headerRightText: {
    fontFamily: fonts.medium_font,
    fontSize: 16,
    color: colors.font_color,
    opacity: 0.8,
  },
  colorBoxView: {
    width: screenWidth / 2.5,
    height: screenHeight / 24,
    borderWidth: 3,
    alignSelf: 'center',
    borderRadius: 5,
    borderColor: '#FF6536',
    borderStyle: 'dotted',
    justifyContent: 'center',
    alignItems: 'center',
  },
  colorBoxText: {
    fontFamily: fonts.medium_font,
    fontSize: 14,
    color: '#FF6536',
  },
  boxBigImageStyle: {
    width: screenWidth - 20,
    height: screenHeight / 3.95,
    alignSelf: 'center',
    marginTop: -25,
  },
  shareImageStyle: {
    width: 20,
    height: 20,
  },
  adHeaderText: {
    marginTop: 10,
    fontFamily: fonts.regular_font,
    fontSize: 14,
    color: '#FF6536',
    textAlign: 'center',
  },
  horizontalbuttonStyle: {
    width: screenWidth / 4.5,
    height: screenHeight / 24,
    backgroundColor: colors.white_color,
    borderRadius: 100,
    marginLeft: 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.4,
    elevation: 5,
    borderColor: '#2E2E2E',
  },
  horizontalbuttonText: {
    fontFamily: fonts.medium_font,
    fontSize: 14,
    color: colors.font_color,
  },
  productBoxView: {
    width: screenWidth - 40,
    height: screenHeight / 6,
    backgroundColor: colors.white_color,
    elevation: 5,
    borderRadius: 16,
    borderWidth: 0.5,
    borderColor: '#cbcdce',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  boxImageStyle: {
    width: screenWidth / 2.95,
    height: screenHeight / 6.5,
    borderRadius: 16,
    overflow: 'hidden',
    marginLeft: 10,
  },
  filterImageStyle: {
    width: 20,
    height: 20,
    marginBottom: 7.5,
    marginTop: 7.5,
  },
  bgImageStyle: {
    width: 24,
    height: 24,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center'
  },
  imageTextStyle: {
    fontFamily: fonts.semibold_font,
    fontSize: 8,
    color: colors.white_color,
    textAlign: 'center',
  },
  roundView: {
    width: screenWidth / 2.75,
    height: screenHeight / 24,
    backgroundColor: colors.white_color,
    borderWidth: 1,
    borderColor: '#2E2E2E',
    opacity: 0.5,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  filterSizeText: {
    fontFamily: fonts.regular_font,
    fontSize: 12,
    color: colors.font_color,
    paddingLeft: 15,
    lineHeight: 35,
  },
  headerView: {
    width: screenWidth - 40,
    alignSelf: 'center',
    flexDirection: 'row',
  },
  filterText: {
    fontFamily: fonts.semibold_font,
    fontSize: 14,
    color: colors.font_color,
    paddingLeft: 15,
  },
  filterImageStle: {
    width: 16,
    height: 16,
  },
  // headerText: {
  //   fontFamily: fonts.medium_font,
  //   fontSize: 14,
  //   color: colors.font_color,
  //   margin: 15,
  // },
  productView: {
    marginTop: 10,
  },
  filterStarImageStyle: {
    width: 16,
    height: 16,
    position: 'absolute',
    bottom: -2.5,
    right: -5,
  },
  boxImageStyles: {
     width: screenWidth - 20,
    height: screenHeight / 4.5,
    alignSelf: 'center',
    marginTop: 20,
    marginLeft: 10,
  }
});
