import React, { useState, useEffect } from 'react';
import { StatusBar, Image, View } from 'react-native'
import styles from './SplashPage,Style';
import { colors } from '../../styles/theme';
const SplashPage = ({ navigation }) => {
  const [red, setRed] = useState(true);
  useEffect(() => {
    const intervalID = setInterval(() => {
      setRed(red => !red);
    }, 1500);
    return () => clearInterval(intervalID);
  }, []);
  setTimeout(() => {
    navigation.navigate('BottomMenu')
  }, 2500);
  return (
    <View style={styles.container}>
      {red == true ?
        <View style={[styles.container, { backgroundColor: '#FFFFFF' }]}>
          <StatusBar hidden={false} animated={true} backgroundColor={colors.white_color} translucent={true} />
          <Image source={require('../../assets/images/splash_logo1.png')} style={styles.imagestyle} resizeMode='contain' />
        </View> :
        <View style={styles.container}>
          <StatusBar hidden={false} animated={true} backgroundColor={colors.primary_color} translucent={true} />
          <Image source={require('../../assets/images/splash_logo1.png')} style={styles.imagestyle} resizeMode='contain' />
        </View>
      }
    </View>
  )
}

export default SplashPage;