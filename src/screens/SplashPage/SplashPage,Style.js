import {StyleSheet, Dimensions} from 'react-native';
import {fonts, colors} from '../../styles/theme';
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary_color,
    justifyContent: 'center',
    alignSelf:'center'
  },
  imagestyle:{
    width:screenWidth,
    height:screenHeight/2.5
  }
})