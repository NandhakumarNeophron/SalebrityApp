import React, {useRef} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StatusBar,
  ScrollView,
  TextInput,
  FlatList,
  Animated,
  Dimensions,
} from 'react-native';
import StepIndicator from 'react-native-step-indicator';
import Divider from '../../components/Divider';
import IconI from 'react-native-vector-icons/Ionicons';
import IconM from 'react-native-vector-icons/MaterialIcons';
import styles from './Payment.Style';
import commonStyle from '../../styles/common.Style';
import Slider from 'react-native-sliders';
import {colors} from '../../styles/theme';

const screenWidth = Math.round(Dimensions.get('window').width);

const Payment = ({navigation}) => {
  const labels = ['Cart', 'Order Summary', 'Track'];
  const customStyles = {
    stepIndicatorSize: 25,
    currentStepIndicatorSize: 30,
    separatorStrokeWidth: 2,
    currentStepStrokeWidth: 3,
    stepStrokeCurrentColor: '#fe7013',
    stepStrokeWidth: 3,
    stepStrokeFinishedColor: '#fe7013',
    stepStrokeUnFinishedColor: '#aaaaaa',
    separatorFinishedColor: '#fe7013',
    separatorUnFinishedColor: '#aaaaaa',
    stepIndicatorFinishedColor: '#fe7013',
    stepIndicatorUnFinishedColor: '#ffffff',
    stepIndicatorCurrentColor: '#ffffff',
    stepIndicatorLabelFontSize: 13,
    currentStepIndicatorLabelFontSize: 13,
    stepIndicatorLabelCurrentColor: '#fe7013',
    stepIndicatorLabelFinishedColor: '#ffffff',
    stepIndicatorLabelUnFinishedColor: '#aaaaaa',
    labelColor: '#999999',
    labelSize: 13,
    currentStepLabelColor: '#fe7013',
  };
  const data = [
    {
      name: 'Green Small Sofa',
      image: require('../../assets/images/checkOut1.png'),
      content1: 'GrößeXS',
      content2: 'Farbe:Beige',
      content3: 'Preis',
      amount: '639',
    },
    {
      name: 'Green Small Sofa',
      image: require('../../assets/images/checkOut2.png'),
      content1: 'GrößeXS',
      content2: 'Farbe:Beige',
      content3: 'Preis',
      amount: '20',
    },
    {
      name: 'Green Small Sofa',
      image: require('../../assets/images/checkOut3.png'),
      content1: 'GrößeXS',
      content2: 'Farbe:Beige',
      content3: 'Preis',
      amount: '420',
    },
  ];
  const [value, setValue] = React.useState([0, 50, 100]);
  const [currentPosition, setPosition] = React.useState(1);
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="transparent" barStyle="dark-content" />
      <View style={styles.topViewStyle}>
        <View style={[styles.topLineViewStyle]}>
          <Image
            source={require('../../assets/images/logo.png')}
            style={styles.logoStyle}
            resizeMode="contain"
          />
        </View>
        <View
          style={[commonStyle.row, styles.topViews, commonStyle.padhorzontal]}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <IconI color="black" name="chevron-back-circle" size={25} />
          </TouchableOpacity>
        </View>
      </View>
      <ScrollView>
        <View style={[commonStyle.padhorzontal, styles.padBottom]}>
          <Text style={styles.heading}>Bestellung</Text>
          <View
            style={{
              height: 100,
              flexDirection: 'column',
              justifyContent: 'center',
              marginTop: 20,
            }}>
            <View style={[commonStyle.row, commonStyle.rowCenter]}>
              <View style={[commonStyle.row]}>
                <View style={styles.firstStep} />
                <View style={styles.scondStep} />
              </View>
              <View style={styles.firstThumb}>
                <Text style={styles.thumbStyle}>Versand</Text>
                <View style={styles.thumb} />
                <IconI color="#FF6536" name="checkmark-sharp" size={15} />
              </View>
              <View style={styles.scondThumb}>
                <Text style={styles.thumbStyle}>Bezahlung</Text>
                <View style={styles.thumb} />
                <IconI color="#FF6536" name="checkmark-sharp" size={15} />
              </View>
              <View style={styles.thirdThumb}>
                <Text style={styles.thumbStyle}>Bestatigen</Text>
                <View
                  style={{...styles.thumb, backgroundColor: colors.black_color}}
                />
                <IconI color="black" name="checkmark-sharp" size={15} />
              </View>
            </View>
          </View>
          {/* <StepIndicator
            customStyles={customStyles}
            currentPosition={currentPosition}
            labels={labels}
            direction="horizontal"
            stepCount="3"
            renderLabel={({label, stepStatus}) => {
              return (
                <View>
                  <Text>{label}</Text>
                  <View>
                    <Text>{stepStatus}</Text>
                  </View>
                </View>
              );
            }}
          /> */}
          <Text style={styles.subheading}>Kauf abschlieben</Text>
          <Text style={styles.content}>
            Keine Ruckgabe mehr moglich. Auber die ware wurde absichtlich
            beschadigt verkauuft.
          </Text>
          <View style={{marginTop: 18}}>
            <Image
              source={require('../../assets/images/Card.png')}
              style={styles.cardStyle}
              resizeMode="stretch"
            />
            {/* <View style={styles.newBank}> */}
            <Text style={styles.newBank}>New Bank</Text>
            <Text style={styles.cardNo}>4154 7950 0315 5088</Text>
            <Text style={styles.cardName}>Maria Lee</Text>
            {/* </View> */}
          </View>
          <Text style={{...styles.subheading}}>Gesamtbetrag</Text>
          <View
            style={[commonStyle.row, commonStyle.spaceBtw, styles.contentView]}>
            <Text style={styles.content1}>Produkte</Text>
            <Text style={styles.amount}>420 €</Text>
          </View>
          <Divider />
          <View
            style={[commonStyle.row, commonStyle.spaceBtw, styles.contentView]}>
            <Text style={styles.content1}>Versand</Text>
            <Text style={styles.amount}>6 €</Text>
          </View>
          <Divider />
          <View
            style={[commonStyle.row, commonStyle.spaceBtw, styles.contentView]}>
            <Text style={styles.totalText}>Gesamtbetrag</Text>
            <Text style={styles.amount}>1085 €</Text>
          </View>
          <Divider />
          <TouchableOpacity
            onPress={() => navigation.navigate('Page3')}
            style={styles.button}>
            <Text style={styles.buttonText}>Bestellen</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};
export default Payment;
