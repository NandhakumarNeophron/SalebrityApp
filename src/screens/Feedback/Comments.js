import React from 'react';
import {View, Text, FlatList, Dimensions} from 'react-native';
import ProgressBar from 'react-native-progress/Bar';
import {fonts, colors} from '../../styles/theme';
import commonStyle from '../../styles/common.Style';
import styles from './Feedback.Style';
import ProfilePicture from 'react-native-profile-picture';
import Stars from '../SuchenFiltern/Starts';
import Divider from '../../components/Divider';
const screenWidth = Math.round(Dimensions.get('window').width);
const Comments = props => {
  const start_data = [1, 2, 3, 4, 5];
  const data = [
    {
      profile: require('../../assets/images/cmt1.png'),
      name: 'Sally Morey',
      date: '30  May, 2023',
      content:
        'Lorem Ipsum ist ein einfacher Demo-Text für die Print- und Schriftindustrie. Lorem Ipsum ist in der Industrie bereits der ',
    },
    {
      profile: require('../../assets/images/cmt2.png'),
      name: 'Matthew Perry',
      date: '30  May, 2023',
      content:
        'Lorem Ipsum ist ein einfacher Demo-Text für die Print- und Schriftindustrie. Lorem Ipsum ist in der Industrie bereits der ',
    },
  ];
  return (
    <View>
      <FlatList
        data={data}
        style={{marginTop: 15}}
        renderItem={({item: rowData}) => {
          return (
            <View style={{marginTop: 15}}>
              <View>
                <View style={commonStyle.row}>
                  <ProfilePicture
                    isPicture={true}
                    requirePicture={rowData.profile}
                    shape="circle"
                    pictureStyle={styles.pictureStyle}
                  />
                  <View style={[styles.commentBox]}>
                    <View style={[commonStyle.row, commonStyle.spaceBtw]}>
                      <Text style={styles.commentTitle}>{rowData.name}</Text>
                      <Stars
                        marginDisable={true}
                        data={start_data}
                        size={10}
                        end={4}
                      />
                    </View>
                    <Text style={styles.commentTime}>{rowData.date}</Text>
                    <Text style={styles.commentContent}>{rowData.content}</Text>
                  </View>
                </View>
                <View style={styles.commentArrow} />
              </View>
              <View style={{marginTop: 15}}>
                <Divider />
              </View>
            </View>
          );
        }}
        keyExtractor={(item, index) => index}
      />
    </View>
  );
};

export default Comments;
