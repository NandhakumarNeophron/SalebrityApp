import React from 'react';
import {View, Text, StyleSheet, Dimensions} from 'react-native';
import ProgressBar from 'react-native-progress/Bar';
import {fonts, colors} from '../../styles/theme';
import commonStyle from '../../styles/common.Style';
import styles from './Feedback.Style';
import Divider from '../../components/Divider';
const screenWidth = Math.round(Dimensions.get('window').width);
const CustomProgressBar = props => {
  return (
    <View>
      <View
        style={[
          commonStyle.row,
          commonStyle.alignCenter,
          styless.space,
          commonStyle.spaceBtw,
        ]}>
        <Text style={styless.leftText}>{props?.item.count ?? '0'} Star</Text>
        <ProgressBar
          borderRadius={10}
          borderColor="#A03232"
          color="#FF6536"
          unfilledColor="#DADADA"
          height={8}
          progress={props?.item.value}
          width={screenWidth * 0.7}
          style={styles.progressBar}
        />
        <Text style={styless.rightText}>{props?.item.percentage ?? '0%'}</Text>
      </View>
      <Divider />
    </View>
  );
};

// Stars.propTypes = {
//   data: PropTypes.array.isRequired,
//   endTitle: PropTypes.string,
// };
export default CustomProgressBar;
const styless = StyleSheet.create({
  leftText: {
    fontWeight: '500',
    fontSize: 12,
    lineHeight: 22,
    letterSpacing: -0.408,
    opacity: 0.8,
    color: colors.font_color,
    fontFamily: fonts.medium_font,
  },
  rightText: {
    fontWeight: '700',
    fontSize: 12,
    lineHeight: 22,
    textAlign: 'right',
    letterSpacing: -0.408,
    color: colors.font_color,
    fontFamily: fonts.bold_font,
  },
  space: {
    marginVertical: 15,
  },
});
