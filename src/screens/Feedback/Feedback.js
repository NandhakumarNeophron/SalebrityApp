import React, {useRef} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StatusBar,
  ScrollView,
  TextInput,
  FlatList,
  AppRegistry,
  Animated,
  Dimensions,
  useWindowDimensions,
} from 'react-native';
import Slider from 'react-native-sliders';
import ProgressBar from 'react-native-progress/Bar';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import Divider from '../../components/Divider';
import CustomProgressBar from './ProgressBar';
import PagerView from 'react-native-pager-view';
// import RBSheet from 'react-native-raw-bottom-sheet';
import Stars from '../SuchenFiltern/Starts';
import styles from './Feedback.Style';
import commonStyle from '../../styles/common.Style';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import IconI from 'react-native-vector-icons/Ionicons';
import {colors} from '../../styles/theme';
import Comments from './Comments';
import BottomSheet from './BottomSheet';
const screenWidth = Math.round(Dimensions.get('window').width);

const renderScene = SceneMap({
  first: Comments,
  second: Comments,
});
const Feedback = ({navigation}) => {
  const start_data1 = [1, 2, 3, 4, 5];
  // const refRBSheet = useRef();
  const start_data = [
    {
      percentage: '58%',
      count: 5,
      value: 0.58,
    },
    {
      percentage: '25%',
      count: 4,
      value: 0.25,
    },
    {
      percentage: '0%',
      count: 3,
      value: 0.04,
    },
    {
      percentage: '8%',
      count: 2,
      value: 0.08,
    },
    {
      percentage: '8%',
      count: 1,
      value: 0.08,
    },
  ];
  const tabs = [
    {key: 'first', name: 'First'},
    {key: 'second', name: 'Second'},
  ];
  const [routes] = React.useState([
    {key: 'first', title: 'Top'},
    {key: 'second', title: 'Most Recent'},
  ]);
  const [index, setIndex] = React.useState(0);
  const [selectTab, setTab] = React.useState(0);
  const layout = useWindowDimensions();
  const renderTabBar = props => (
    <TabBar
      {...props}
      tabStyle={{alignItems: 'flex-start', width: 'auto'}}
      gap={10}
      indicatorStyle={{backgroundColor: 'red'}}
      labelStyle={styles.tabbarlabel}
      style={{
        backgroundColor: 'white',
        width: 'auto',
        justifyContent: 'flex-start',
      }}
    />
  );
  const renderBottomSheet = () => (
    <View>
      <Text>Sortieren</Text>
    </View>
  );
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="transparent" barStyle="dark-content" />
      <View style={styles.topViewStyle}>
        <View style={[styles.topLineViewStyle]}>
          <Image
            source={require('../../assets/images/logo.png')}
            style={styles.logoStyle}
            resizeMode="contain"
          />
        </View>
      </View>
      <ScrollView>
        <View style={[commonStyle.padhorzontal, styles.padBottom]}>
          <View style={[commonStyle.row, commonStyle.spaceBtw]}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <IconI color="black" name="chevron-back-circle" size={25} />
            </TouchableOpacity>
            <Text style={styles.heading}>Bewertungen</Text>
            <View />
          </View>

          <View style={[commonStyle.row, commonStyle.rowCenter]}>
            <Text style={styles.rating1}>4.16</Text>
            <Text style={styles.rating2}>/</Text>
            <Text style={styles.rating3}>5.0</Text>
          </View>
          <View
            style={[
              commonStyle.row,
              commonStyle.rowCenter,
              styles.reduceMargin,
            ]}>
            <Stars data={start_data1} size={10} end={4} />
          </View>
          <View style={[commonStyle.row, commonStyle.rowCenter]}>
            <Text style={styles.normalStyle}>12 Bewerungen</Text>
          </View>
          <FlatList
            data={start_data}
            style={{marginTop: 15}}
            renderItem={({item: rowData}) => {
              return <CustomProgressBar item={rowData} />;
            }}
            keyExtractor={(item, index) => index}
          />
          <View style={{marginTop: 1}}>
            <Divider />
          </View>
          <TabView
            navigationState={{index, routes}}
            renderScene={renderScene}
            onIndexChange={setIndex}
            renderTabBar={renderTabBar}
            style={{width: screenWidth / 2, marginTop: 10}}
          />
          {index == 0 && <Comments />}
          {index == 1 && <Comments />}
        </View>
      </ScrollView>
    </View>
  );
};
const _renderTabBar = props => {
  const inputRange = props.navigationState.routes.map((x, i) => i);

  return (
    <View style={styles.tabBar}>
      {props.navigationState.routes.map((route, i) => {
        const opacity = props.position.interpolate({
          inputRange,
          outputRange: inputRange.map(inputIndex =>
            inputIndex === i ? 1 : 0.5,
          ),
        });

        return (
          <TouchableOpacity style={styles.tabItem}>
            <Animated.Text style={{opacity}}>{route.title}</Animated.Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
};
export default Feedback;
