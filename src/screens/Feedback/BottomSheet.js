import React from 'react';
import { View, Text, FlatList, Dimensions, ScrollView } from 'react-native';
import ProgressBar from 'react-native-progress/Bar';
import { fonts, colors } from '../../styles/theme';
import commonStyle from '../../styles/common.Style';
import { RadioButton } from 'react-native-paper';
import styles from './Feedback.Style';
import Divider from '../../components/Divider';
const screenWidth = Math.round(Dimensions.get('window').width);
const BottomSheet = props => {
  const [checked, setChecked] = React.useState('Relevanz');
  const data = [
    {
      name: 'Relevanz',
    },
    {
      name: 'Beliebt',
    },
    {
      name: 'Preis absteigend',
    },
    {
      name: 'Preis aufsteigend',
    },
  ];
  return (
    <View>
      <ScrollView>
        <View style={styles.sheetContainer}>
          <Text style={styles.sheetTitle}>Sortieren</Text>
          <FlatList
            data={data}
            style={{ marginTop: 15 }}
            renderItem={({ item: i }) => {
              return (
                <View style={[commonStyle.padhorzontal]}>
                  <View
                    style={[
                      commonStyle.row,
                      commonStyle.spaceBtw,
                      { alignItems: 'center' },
                    ]}>
                    <Text style={styles.sheetContent}>{i.name}</Text>
                    <View
                      style={{
                        backgroundColor: '#FEF2E6',
                        borderRadius: 100,
                        height: 20,
                        width: 20,
                        alignItems: 'center',
                        justifyContent: 'center',
                        marginTop: 5,
                      }}>
                      <RadioButton
                        value="first"
                        color="#FF6536"
                        uncheckedColor="#2e2e2e80"
                        status={checked === i.name ? 'checked' : 'unchecked'}
                        onPress={() => setChecked(i.name)}
                      />
                    </View>
                  </View>
                  <View style={styles.sheetSpace}>
                    <Divider />
                  </View>
                </View>
              );
            }}
            keyExtractor={(item, index) => index}
          />
          <View style={styles.button}>
            <Text style={styles.buttonText}>24 Ergebnisse</Text>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default BottomSheet;
// const styless = StyleSheet.create({});
