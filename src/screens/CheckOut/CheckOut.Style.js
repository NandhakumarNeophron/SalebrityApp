import {StyleSheet, Dimensions} from 'react-native';
import {fonts, colors, fontsMaven} from '../../styles/theme';
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white_color,
  },
  topViewStyle: {
    width: screenWidth,
    // height: screenHeight / 4,
    backgroundColor: colors.white_color,
    marginBottom: 15,
  },
  topLineViewStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 55,
    // marginLeft: 20,
  },
  topViews: {
    marginTop: -30,
  },
  logoStyle: {
    width: 86,
    height: 86,
    marginTop: -60,
  },
  iconStyle: {
    width: 24,
    height: 24,
  },
  heading: {
    // fontWeight: '700',
    fontSize: 20,
    marginTop: 25,
    lineHeight: 22,
    textAlign: 'center',
    // letterSpacing: -0.408,
    color: colors.font_color,
    fontFamily: fonts.bold_font,
  },
  cardImage: {
    borderRadius: 15,
    borderWidth: 2,
    height: 97,
    width: 99,
    borderColor: 'solid #FEF2E6',
  },
  cardTitle: {
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 22,
    textAlign: 'center',
    letterSpacing: -0.408,
    color: colors.font_color,
    fontFamily: fonts.medium_font,
  },
  cardTitle: {
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 22,
    textAlign: 'center',
    letterSpacing: -0.408,
    color: colors.font_color,
    fontFamily: fonts.medium_font,
  },
  cardcontent: {
    fontWeight: '400',
    fontSize: 12,
    lineHeight: 22,
    textAlign: 'center',
    letterSpacing: -0.408,
    color: '#979797',
    fontFamily: fonts.regular_font,
  },
  cardSection: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'space-evenly',
    marginLeft: 8,
  },
  cardSection1: {
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    paddingBottom: 2,
  },
  cardAmount: {
    fontWeight: '600',
    fontSize: 16,
    lineHeight: 22,
    letterSpacing: -0.408,
    color: colors.font_color,
    fontFamily: fonts.semibold_font,
  },
  cardSpace: {
    marginTop: 20,
    marginBottom: 8,
  },
  Totaltext: {
    fontWeight: '600',
    fontSize: 18,
    lineHeight: 22,
    letterSpacing: -0.408,
    color: colors.font_color,
    fontFamily: fonts.semibold_font,
  },
  Totaltextcontent: {
    fontWeight: '400',
    fontSize: 10,
    lineHeight: 22,
    letterSpacing: -0.408,
    color: colors.font_color,
    opacity: 0.5,
    fontFamily: fonts.regular_font,
  },
  totalView: {
    marginTop: 25,
  },
  button: {
    height: 40,
    width: 215,
    borderRadius: 15,
    borderWidth: 1,
    borderColor: colors.font_color,
    backgroundColor: '#DDCFC6',
    alignSelf: 'center',
    justifyContent: 'center',
    marginTop: 30,
    marginBottom: 20,
  },
  buttonText: {
    fontWeight: '400',
    fontSize: 16,
    lineHeight: 22,
    letterSpacing: -0.408,
    color: colors.font_color,
    textAlign: 'center',
    fontFamily: fonts.semibold_font,
  },
});
