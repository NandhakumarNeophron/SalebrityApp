import React, {useRef} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StatusBar,
  ScrollView,
  TextInput,
  FlatList,
  AppRegistry,
  Animated,
  Dimensions,
  useWindowDimensions,
} from 'react-native';

import Divider from '../../components/Divider';
import IconI from 'react-native-vector-icons/Ionicons';
import IconF from 'react-native-vector-icons/FontAwesome5';
import styles from './CheckOut.Style';
import commonStyle from '../../styles/common.Style';

import {colors} from '../../styles/theme';

const screenWidth = Math.round(Dimensions.get('window').width);

const CheckOut = ({navigation}) => {
  const data = [
    {
      name: 'Green Small Sofa',
      image: require('../../assets/images/checkOut1.png'),
      content1: 'GrößeXS',
      content2: 'Farbe:Beige',
      content3: 'Preis',
      amount: '639',
    },
    {
      name: 'Green Small Sofa',
      image: require('../../assets/images/checkOut2.png'),
      content1: 'GrößeXS',
      content2: 'Farbe:Beige',
      content3: 'Preis',
      amount: '20',
    },
    {
      name: 'Green Small Sofa',
      image: require('../../assets/images/checkOut3.png'),
      content1: 'GrößeXS',
      content2: 'Farbe:Beige',
      content3: 'Preis',
      amount: '420',
    },
  ];
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="transparent" barStyle="dark-content" />
      <View style={styles.topViewStyle}>
        <View style={[styles.topLineViewStyle]}>
          <Image
            source={require('../../assets/images/logo.png')}
            style={styles.logoStyle}
            resizeMode="contain"
          />
        </View>
        <View
          style={[commonStyle.row, styles.topViews, commonStyle.padhorzontal]}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <IconI color="black" name="chevron-back-circle" size={25} />
          </TouchableOpacity>
        </View>
      </View>
      <ScrollView>
        <View style={[commonStyle.padhorzontal, styles.padBottom]}>
          <Text style={styles.heading}>Einkaufswagen</Text>
          <FlatList
            data={data}
            style={{marginTop: 15}}
            renderItem={({item: i}) => {
              return (
                <View>
                  <View
                    style={[
                      commonStyle.row,
                      commonStyle.spaceBtw,
                      styles.cardSpace,
                    ]}>
                    <View style={[commonStyle.row]}>
                      <Image
                        style={styles.cardImage}
                        source={i.image}
                        resizeMode="contain"
                      />
                      <View style={styles.cardSection}>
                        <Text style={styles.cardTitle}>{i.name}</Text>
                        <Text style={styles.cardcontent}>{i.content1}</Text>
                        <Text style={styles.cardcontent}>{i.content2}</Text>
                        <Text style={styles.cardcontent}>{i.content3}</Text>
                      </View>
                    </View>
                    <View style={styles.cardSection1}>
                      <Image
                        source={require('../../assets/images/trash.png')}
                        style={styles.iconStyle}
                        resizeMode="contain"
                      />
                      {/* <IconF color="#979797" name="trash" size={20} /> */}
                      <Text style={styles.cardAmount}>{i.amount} €</Text>
                    </View>
                  </View>
                  <Divider />
                </View>
              );
            }}
            keyExtractor={(item, index) => index}
          />
          <View style={styles.totalView}>
            <Divider />
            <View
              style={{
                ...commonStyle.row,
                ...commonStyle.spaceBtw,
                marginTop: 15,
                marginBottom: 10,
              }}>
              <View>
                <Text style={styles.Totaltext}>Gesamtbetrag</Text>
                <Text style={styles.Totaltextcontent}>Inkl. MwSt</Text>
              </View>

              <Text style={styles.cardAmount}>1079 €</Text>
            </View>
            <Divider />
          </View>
          <TouchableOpacity
            onPress={() => navigation.navigate('Payment')}
            style={styles.button}>
            <Text style={styles.buttonText}>Bezahlen</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};
export default CheckOut;
