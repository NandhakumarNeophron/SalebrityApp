export const fonts = {
  regular_font: 'Inter-Regular',
  semibold_font: 'Inter-SemiBold',
  bold_font: 'Inter-Bold',
  medium_font: 'Inter-Medium',
  light_font: 'Inter-Light',
};
export const fontsMaven = {
  regular_font: 'MavenPro-Regular',
  semibold_font: 'MavenPro-SemiBold',
  bold_font: 'MavenPro-Bold',
  medium_font: 'MavenPro-Medium',
  light_font: 'MavenPro-Regular',
};
export const fontPoppins = {
  regular_font: 'Poppins-Regular',
  semibold_font: 'Poppins-SemiBold',
  bold_font: 'Poppins-Bold',
  extra_bold_font: 'Poppins-ExtraBold',
  medium_font: 'Poppins-Medium',
  light_font: 'Poppins-Regular',
};
export const colors = {
  white_color: '#FFFFFF',
  black_color: '#000000',
  primary_color: '#DDCFC6',
  bg_color: '#FEFEFE',
  orange_color: '#FF6536',
  font_color: '#2E2E2E',
  transparent: 'transparent',
};
