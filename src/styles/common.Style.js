import {StyleSheet, Dimensions} from 'react-native';
import {fonts, colors} from './theme';
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
export default StyleSheet.create({
  row: {
    flexDirection: 'row',
  },
  columm: {
    flexDirection: 'column',
  },
  columnCenter: {
    alignItems: 'center',
  },
  spaceBtw: {
    justifyContent: 'space-between',
  },
  rowCenter: {
    justifyContent: 'center',
  },
  padhorzontal: {
    paddingHorizontal: 20,
  },
  alignCenter: {
    alignItems: 'center',
  },

  topViewStyle: {
    width: screenWidth,
    // height: screenHeight / 4,
    backgroundColor: colors.white_color,
    marginBottom: 15,
  },
  topLineViewStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: 55,
    // marginLeft: 20,
  },
  topViews: {
    marginTop: -30,
  },
  logoStyle: {
    width: 86,
    height: 86,
    marginTop: -60,
  },
  sheetBox: {
    backgroundColor: colors.primary_color,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    height: screenHeight / 2.2,
  },
});
