import React from 'react';
import {View, StyleSheet} from 'react-native';
import {colors} from '../styles/theme';
const Divider = ({navigation}) => {
  return (
    <View
      style={{
        borderBottomColor: colors.font_color,
        opacity: 0.5,
        borderBottomWidth: StyleSheet.hairlineWidth,
      }}
    />
  );
};
export default Divider;
