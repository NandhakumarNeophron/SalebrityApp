import React from 'react';
import { View, Image } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
const Tab = createBottomTabNavigator();
import styles from './BottomMeny.Style';
import SalesStack from './Sales';
import EventsStack from './Events';
import ProfileStack from './Profile';
import MailsStack from './Mail';
import WishStack from './Wish';
const BottomMenu = () => {
  return (
    <Tab.Navigator
      initialRouteName="Events"
      screenOptions={{
        tabBarShowLabel: false,
        tabBarStyle: {
          backgroundColor: '#DDCFC6',
          width: '99%',
          height: 70,
          borderRadius: 15,
          position: 'relative',
          elevation: 0,
          alignSelf: 'center',
          margin: 1,
          overflow: 'hidden',
          borderTopColor:'black',
          borderBottomWidth: 1,
          alignContent: 'center',
          borderLeftWidth: 1,
          borderRightWidth: 1,
          borderTopWidth: 1,
          ...styles.shadow,
        },
      }}>
      <Tab.Screen
        name="Connect"
        component={MailsStack}
        options={{
          headerShown: false,
          tabBarIcon: ({ focused }) => (
            <View
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                top: 0,
              }}>
              <Image
                source={require('../assets/images/mail_icon.png')}
                resizeMode="contain"
                style={{
                  width: 24,
                  height: 24,
                  tintColor: focused ? '#FE9100' : '#2E2E2E',
                }}
              />
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="GNote"
        component={WishStack}
        options={{
          headerShown: false,
          tabBarIcon: ({ focused }) => (
            <View
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                top: 0,
              }}>
              <Image
                source={require('../assets/images/wish_icon.png')}
                resizeMode="contain"
                style={{
                  width: 24,
                  height: 24,
                  tintColor: focused ? '#FE9100' : '#2E2E2E',
                }}
              />
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="Sales"
        component={SalesStack}
        options={{
          headerShown: false,
          tabBarIcon: ({ focused }) => (
            <View
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                top: 0,
              }}>
              <Image
                source={require('../assets/images/sales_icon.png')}
                resizeMode="contain"
                style={{ width: 64, height: 64 }}
              />
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={ProfileStack}
        options={{
          headerShown: false,
          tabBarIcon: ({ focused }) => (
            <View
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                top: 0,
              }}>
              <Image
                source={require('../assets/images/clock_icon.png')}
                resizeMode="contain"
                style={{
                  width: 24,
                  height: 24,
                  tintColor: focused ? '#FE9100' : '#2E2E2E',
                }}
              />
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="Events"
        component={EventsStack}
        options={{
          headerShown: false,
          tabBarIcon: ({ focused }) => (
            <View
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                top: 0,
              }}>
              <Image
                source={require('../assets/images/group_people_icon.png')}
                resizeMode="contain"
                style={{
                  width: 24,
                  height: 24,
                  tintColor: focused ? '#FE9100' : '#2E2E2E',
                }}
              />
            </View>
          ),
        }}
      />
    </Tab.Navigator>
  );
};
export default BottomMenu;
