import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Page3 from '../screens/Page3/Page3';
// import ActionPage1 from '../screens/ActionPage/ActionPage1';
import StoreView from '../screens/StoreView/StoreView';
import StoreView1 from '../screens/StoreView/StoreView1';
import StoreDetails from '../screens/StoreView/StoreDetails';
import ActionPage1 from '../screens/ActionPage/ActionPage1';
import SuchenFiltern from '../screens/SuchenFiltern/SuchenFiltern';
import Feedback from '../screens/Feedback/Feedback';
import CheckOut from '../screens/CheckOut/CheckOut';
import Payment from '../screens/Payment/payment';
const Stack = createNativeStackNavigator();
function SalesStack() {
  return (
    <Stack.Navigator
      initialRouteName="SalesHome"
      screenOptions={{headerShown: false}}>
      <Stack.Screen name="SalesHome" component={ActionPage1} />
      <Stack.Screen name="StoreView" component={StoreView} />
      <Stack.Screen name="StoreView1" component={StoreView1} />
      <Stack.Screen name="StoreDetails" component={StoreDetails} />
      <Stack.Screen name="Page3" component={Page3} />
      <Stack.Screen name="FilterView" component={SuchenFiltern} />
      <Stack.Screen name="Feedback" component={Feedback} />
      <Stack.Screen name="CheckOut" component={CheckOut} />
      <Stack.Screen name="Payment" component={Payment} />
    </Stack.Navigator>
  );
}
export default SalesStack;
