import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import ActionPage2 from '../screens/ActionPage/ActionPage2';
const Stack = createNativeStackNavigator();

function ProfileStack() {
  return (
    <Stack.Navigator
      initialRouteName="ProfileHome"
      screenOptions={{headerShown: false}}>
      <Stack.Screen name="ProfileHome" component={ActionPage2} />
      {/* <Stack.Screen name="Details" component={DetailsScreen} /> */}
    </Stack.Navigator>
  );
}
export default ProfileStack;
