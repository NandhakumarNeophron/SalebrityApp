import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import Page1 from '../screens/Page1/Page1';
import Feedback from '../screens/Feedback/Feedback';
const Stack = createNativeStackNavigator();

function MailsStack() {
  return (
    <Stack.Navigator
      initialRouteName="Page1"
      screenOptions={{headerShown: false}}>
      <Stack.Screen name="Page1" component={Page1} />
      {/* <Stack.Screen name="Details" component={DetailsScreen} /> */}
    </Stack.Navigator>
  );
}
export default MailsStack;
