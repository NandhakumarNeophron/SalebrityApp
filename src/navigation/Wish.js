import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import Page2 from '../screens/Page2/Page2';

import StoreView1 from '../screens/StoreView/StoreView1';
const Stack = createNativeStackNavigator();

function WishStack() {
  return (
    <Stack.Navigator
      initialRouteName="WishHome"
      screenOptions={{headerShown: false}}>
      <Stack.Screen name="WishHome" component={Page2} />
      {/* <Stack.Screen name="Details" component={DetailsScreen} /> */}
    </Stack.Navigator>
  );
}
export default WishStack;
